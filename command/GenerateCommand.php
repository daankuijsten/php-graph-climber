<?php
declare(strict_types=1);

namespace Command;

use Exception;
use PhpGraphClimber\Generate;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'generate';

    protected function configure(): void
    {
        $this->addArgument('file', InputArgument::REQUIRED, 'The path of the file to create tests for');
        $this->setDescription('Generate a test file by supplying a PHP class file');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $filePath = $input->getArgument('file');

        try {

            // Check if the file is given as an absolute path and if the file exists
            if (strpos($filePath, DIRECTORY_SEPARATOR) === 0 && file_exists($filePath)) {
                $this->generate($filePath);
                return $this->handleSuccess($output);
            }

            $currentWorkingDirectory = getcwd();

            // Check if the file is given as a relative path and if the file exists
            $filePathRelative = $currentWorkingDirectory . DIRECTORY_SEPARATOR . $filePath;
            if (file_exists($filePathRelative)) {
                $this->generate($filePath);
                return $this->handleSuccess($output);
            }

            $output->writeln('File not found');
            return Command::FAILURE;

        } catch (Exception $e) {
            $output->writeln('Generating tests failed: ' . $e->getMessage());
            return Command::INVALID;
        }
    }

    private function generate(string $filePath): void
    {
        $generate = new Generate();
        $generate->run($filePath);
    }

    private function handleSuccess(OutputInterface $output): int
    {
        $output->writeln('Test files generated!');
        return Command::SUCCESS;
    }

}