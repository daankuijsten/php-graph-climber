<?php
declare(strict_types=1);

require __DIR__ . "/vendor/autoload.php";

use PhpGraphClimber\Generate;

//$filePath = './test/Fixtures/Object_/Object_.php';
//$filePath = './test/Fixtures/Branching/If_.php';
$filePath = './test/Fixtures/Return_/Return_.php';

$generate = new Generate();
$generate->run($filePath);

