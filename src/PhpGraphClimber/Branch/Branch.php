<?php
declare(strict_types=1);

namespace PhpGraphClimber\Branch;

use Exception;
use PhpGraphClimber\EndStatement\EndStatement;
use PhpGraphClimber\Parameter\ParameterGroupCollection;
use PhpGraphClimber\PossibleInput\PossibleInputParametersCollection;
use PhpGraphClimber\Variable\Variable;
use PHPUnit\TextUI\XmlConfiguration\VariableCollection;

class Branch
{
    /**
     * @var ParameterGroupCollection
     */
    private $parameterGroupCollection;

    /**
     * @var array
     */
    private $branches = [];

    /**
     * @var array
     */
    private $variables = [];

    /**
     * @var array|null
     */
    private $nodeNumbersParentBranch;

    /**
     * @var array
     */
    private $nodeNumbers;

    /**
     * @var VariableCollection
     */
    private $variableCollection;

    /**
     * @var int|null
     */
    private $ifNodeNumber;

    private $leaveNode = 0;

    /**
     * @var bool
     */
    private $end = false;

    private $endStatement;

    /**
     * @var PossibleInputParametersCollection
     */
    private $possibleInputParametersCollection;

    public function __construct(
        array $nodeNumbers,
        ?array $nodeNumbersParentBranch = null
    )
    {
        $this->nodeNumbers = $nodeNumbers;
        $this->nodeNumbersParentBranch = $nodeNumbersParentBranch;
    }

    public function getParameterGroupCollection(): ?ParameterGroupCollection
    {
        return $this->parameterGroupCollection;
    }

    public function setParameterGroupCollection(ParameterGroupCollection $parameterGroupCollection): void
    {
        $this->parameterGroupCollection = $parameterGroupCollection;
    }

    public function getBranches(): array
    {
        return $this->branches;
    }

    public function setBranches(array $branches): void
    {
        $this->branches = $branches;
    }

    public function addBranches(array $branches): void
    {
        foreach($branches as $branch) {
            $this->addBranch($branch);
        }
    }

    public function addBranch(Branch $branch): void
    {
        $this->branches[] = $branch;
    }

    public function addVariable(Variable $variable): void
    {
        $this->variables[] = $variable;
    }

    public function getNodeNumbers(): ?array
    {
        return $this->nodeNumbers;
    }

    public function getNodeNumbersParentBranch(): ?array
    {
        return $this->nodeNumbersParentBranch;
    }

    public function setIfNodeNumber(?int $nodeNumber): void
    {
        $this->ifNodeNumber = $nodeNumber;
    }

    public function getIfNodeNumber(): ?int
    {
        return $this->ifNodeNumber;
    }

    public function countLeaveNode(): void
    {
        $this->leaveNode++;
    }

    public function getLeaveNodes(): int
    {
        return $this->leaveNode;
    }

    public function allNodesLeft(): bool
    {
        $totalNodes = count($this->getNodeNumbers());
        $leaveNodes = $this->getLeaveNodes();
        if ($totalNodes < $leaveNodes) {
            throw new Exception('Left more nodes than possible');
        }

        return $leaveNodes === $totalNodes;
    }

    public function endBranch(): void
    {
        $this->end = true;
    }

    public function setEndStatement($endStatement): void
    {
        $this->endStatement = $endStatement;
    }

    public function getEndStatement(): ?EndStatement
    {
        return $this->endStatement;
    }

    // TODO: rename
    public function getPossibleInputParametersCollection(): PossibleInputParametersCollection
    {
        return $this->possibleInputParametersCollection;
    }

    public function setPossibleInputParametersCollection(PossibleInputParametersCollection $possibleInputParametersCollection): void
    {
        $this->possibleInputParametersCollection = $possibleInputParametersCollection;
    }
    public function addPossibleInputValues(PossibleInputParametersCollection $possibleInputValues): void
    {
        $this->possibleInputParametersCollection[] = $possibleInputValues;
    }

}
