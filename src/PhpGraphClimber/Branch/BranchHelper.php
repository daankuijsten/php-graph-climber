<?php
declare(strict_types=1);

namespace PhpGraphClimber\Branch;

use Nette\PhpGenerator\Method;
use PhpGraphClimber\Parameter\Parameter;
use PhpGraphClimber\Parameter\ParameterGroup;
use PhpGraphClimber\Parameter\ParameterGroupCollection;
use PhpGraphClimber\Parameter\ParameterHelper;

class BranchHelper
{
    /**
     * @var ParameterHelper
     */
    private $parameterHelper;

    public function __construct(ParameterHelper $parameterHelper)
    {
        $this->parameterHelper = $parameterHelper;
    }

    public function firstNodeToBranch(Method $classMethod): Branch
    {
        $parameterGroup = new ParameterGroup();

        foreach($classMethod->getParameters() as $parameter) {
            $parameterNew = new Parameter($parameter->getName());
            $parameterNew->setValue(1);
            $parameterNew->setOriginalValue(1);

            if ($this->parameterHelper->requiresRange($parameter->getType())) {
                $parameterNew->addRange($this->parameterHelper->initialiseRange($parameter->getType()));
            }

            $parameterGroup->add($parameterNew);
        }

        $parameterGroupCollection = new ParameterGroupCollection;
        $parameterGroupCollection->add($parameterGroup);

        $branch = new Branch([null], [null, null]);
        $branch->setParameterGroupCollection($parameterGroupCollection);
        $branch->setIfNodeNumber(null);

        return $branch;
    }

    public function nodeNumbersToBranch(
        array $branchNodeNumbers,
        ?array $branchNodeNumbersParent,
        ParameterGroupCollection $parameterGroupCollection,
        $ifNodeNumber
    ): Branch {
        $branch = new Branch($branchNodeNumbers, $branchNodeNumbersParent);
        $branch->setParameterGroupCollection($parameterGroupCollection);
        $branch->setIfNodeNumber($ifNodeNumber);
        return $branch;
    }

}