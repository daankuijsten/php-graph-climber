<?php
declare(strict_types=1);

namespace PhpGraphClimber;

interface CollectionInterface
{
    public function getAll(): array;
}
