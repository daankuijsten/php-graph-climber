<?php
declare(strict_types=1);

namespace PhpGraphClimber\Container;

use Psr\Container\ContainerInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class Container
{
    private static $container;

    public static function create(): ContainerInterface
    {
        if (isset(self::$container)) {
            return self::$container;
        }

        self::$container = new ContainerBuilder();
        $loader = new YamlFileLoader(self::$container, new FileLocator(__DIR__.'/../../../config'));
        $loader->load('services.yaml');
        self::$container->compile();

        return self::$container;
    }

}