<?php
declare(strict_types=1);

namespace PhpGraphClimber\EndStatement;

class EndStatement
{

    /**
     * @var string
     */
    private $type;

    private $value;

    /**
     * @var string
     */
    private $valueType;

    public function __construct(string $type, string $valueType, $value)
    {
        $this->type = $type;
        $this->valueType = $valueType;
        $this->value = $value;
    }

    public function getValueType(): string
    {
        return $this->valueType;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getValue()
    {
        return $this->value;
    }
}
