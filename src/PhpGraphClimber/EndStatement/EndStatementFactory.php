<?php
declare(strict_types=1);

namespace PhpGraphClimber\EndStatement;

use Exception;
use PhpGraphClimber\ExpressionMap;
use PhpParser\Node;

class EndStatementFactory
{
    public function createEndStatement(Node $node): EndStatementInterface
    {
        $expressionName = self::determineExpressionName($node);

        switch ($expressionName)
        {
            case 'ValueReturn';
                $valueReturn = new ValueReturn();
                $valueReturn->setNode($node);
                return $valueReturn;

            case 'ValueThrow';
                $valueThrow = new ValueThrow();
                $valueThrow->setNode($node);
                return $valueThrow;

            default:
                throw new Exception('Expression name not recognized');
        }

    }

    private static function determineExpressionName($node): string
    {
        if (in_array($node->getType(), ExpressionMap::STOP_EXECUTION_STATEMENTS, true)) {
            if ($node->getType() === 'Stmt_Return') {
                // TODO
                return 'ValueReturn';
            }
            if ($node->getType() === 'Stmt_Throw') {
                // TODO
                return 'ValueThrow';
            }
        }

        throw new Exception('EndStatement not recognized: ' . json_encode($node, JSON_THROW_ON_ERROR));
    }
}