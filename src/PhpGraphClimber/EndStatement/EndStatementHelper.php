<?php
declare(strict_types=1);

namespace PhpGraphClimber\EndStatement;

class EndStatementHelper
{
    public function endStatementReturnsScalar(EndStatement $endStatement): bool
    {
        return $endStatement->getType() === 'return' && is_scalar($endStatement->getValue());
    }

    public function endStatementReturnsNull(EndStatement $endStatement): bool
    {
        return $endStatement->getType() === 'return' && is_null($endStatement->getValue());
    }

    public function endStatementReturnsArray(EndStatement $endStatement): bool
    {
        return $endStatement->getType() === 'return' && is_array($endStatement->getValue());
    }

    public function endStatementReturnsObject(EndStatement $endStatement): bool
    {
        return $endStatement->getType() === 'return' && is_object($endStatement->getValue());
    }

    public function endStatementThrowsException(EndStatement $endStatement): bool
    {
        return $endStatement->getType() === 'exception';
    }

}
