<?php
declare(strict_types=1);

namespace PhpGraphClimber\EndStatement;

use PhpParser\Node;

interface EndStatementInterface
{
    public function setNode(Node $node): void;
    public function createEndStatement(): EndStatement;
}