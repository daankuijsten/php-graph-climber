<?php
declare(strict_types=1);

namespace PhpGraphClimber\EndStatement;

class ThrowEndStatement
{
    public const TYPE = 'type';
    public const MESSAGE = 'message';
    public const CODE = 'code';

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $message;

    /**
     * @var int
     */
    private $code;

    public function __construct(
        string $type,
        ?string $message = null,
        ?int $code = null
    )
    {

        $this->type = $type;
        $this->message = $message;
        $this->code = $code;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function getCode(): ?int
    {
        return $this->code;
    }

    public function getType(): string
    {
        return $this->type;
    }
}
