<?php
declare(strict_types=1);

namespace PhpGraphClimber\EndStatement;

use Exception;
use PhpParser\Node;

class ValueReturn implements EndStatementInterface
{
    // true/false/null
    public const VALUE_TYPE__EXPR_CONST_FETCH = 'Expr_ConstFetch';
    // int
    public const VALUE_TYPE__SCALAR_LNUMBER = 'Scalar_LNumber';
    // float
    public const VALUE_TYPE__SCALAR_DNUMBER = 'Scalar_DNumber';
    // string
    public const VALUE_TYPE__SCALAR_STRING = 'Scalar_String';
    // array
    public const VALUE_TYPE__EXPR_ARRAY = 'Expr_Array';
    // object
    public const VALUE_TYPE__EXPR_NEW = 'Expr_New';

    /**
     * @var Node
     */
    private $node;

    public const TYPE = 'return';

    public function setNode(Node $node): void
    {
        $this->node = $node;
    }

    public function createEndStatement(): EndStatement
    {
        $value = $valueType = null;

        if ($this->node->expr->getType() === self::VALUE_TYPE__EXPR_CONST_FETCH) {
            if ($this->node->expr->name->parts[0] === 'true') {
                $value = true;
                $valueType = 'bool';
            }
            if ($this->node->expr->name->parts[0] === 'false') {
                $value = false;
                $valueType = 'bool';
            }
            if ($this->node->expr->name->parts[0] === 'null') {
                $value = null;
                $valueType = 'null';
            }
        }

        if ($this->node->expr->getType() === self::VALUE_TYPE__SCALAR_LNUMBER) {
            $value = $this->node->expr->value;
            $valueType = 'int';
        }

        if ($this->node->expr->getType() === self::VALUE_TYPE__SCALAR_DNUMBER) {
            $value = $this->node->expr->value;
            $valueType = 'float';
        }

        if ($this->node->expr->getType() === self::VALUE_TYPE__SCALAR_STRING) {
            $value = $this->node->expr->value;
            $valueType = 'string';
        }

        if ($this->node->expr->getType() === self::VALUE_TYPE__EXPR_ARRAY) {
            $value = $this->node->expr->items;
            $valueType = 'array';
        }

        // TODO: only returning "creating a new object" shouldn't be the only case
        if ($this->node->expr->getType() === self::VALUE_TYPE__EXPR_NEW) {
            $value = [$this->node->expr->class->parts[0], $this->node->expr->args];
            $valueType = 'object';
        }

        if (!isset($value, $valueType) && $valueType !== 'null') {
            throw new Exception('Could not determine end statement: ' . json_encode($this->node, JSON_THROW_ON_ERROR));
        }

        return new EndStatement(self::TYPE, $valueType, $value);
    }
}
