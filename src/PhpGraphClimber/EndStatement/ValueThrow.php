<?php
declare(strict_types=1);

namespace PhpGraphClimber\EndStatement;

use Exception;
use PhpParser\Node;
use PhpParser\Node\Scalar;

class ValueThrow implements EndStatementInterface
{

    private const TYPE = 'exception';

    /**
     * @var Node
     */
    private $node;

    public function setNode(Node $node): void
    {
        $this->node = $node;
    }

    public function createEndStatement(): EndStatement
    {
        if (!isset($this->node)) {
            throw new Exception('First run setNode before running createEndStatement');
        }

        if ($this->node->expr->getType() === 'Expr_New') {
            if (!isset($this->node->expr->class->parts[0])) {
                throw new Exception('Could not determine type on end statement: ' . json_encode($this->node->expr, JSON_THROW_ON_ERROR));
            }

            $type = $this->node->expr->class->parts[0];

            $values = [];

            if (isset($this->node->expr->args)) {
                $values = $this->getValuesFromArguments();
            }

            $value = new ThrowEndStatement($type, ...$values);
            return new EndStatement(self::TYPE, $value);
        }

        throw new Exception('Could not determine end statement: ' . json_encode($this->node, JSON_THROW_ON_ERROR));
    }

    private function getValuesFromArguments(): array
    {
        $values = [];
        foreach ($this->node->expr->args as $argument) {
            var_dump($argument->value);
            if ($argument->value instanceof Scalar) {
                $values[] = $this->getValueFromScalar($argument);
                continue;
            }

            $values[] = $this->getValueFromExpression($argument);
        }

        return $values;
    }

    private function getValueFromExpression($argument)
    {
        if (!isset($argument->value->name->parts[0])) {
            throw new Exception('Could not determine value on expression on end statement: ' . json_encode($argument, JSON_THROW_ON_ERROR));
        }

        if (strtolower($argument->value->name->parts[0]) === 'null') {
            return null;
        }

        return $argument->name->parts[0];
    }

    private function getValueFromScalar($argument)
    {
        if (!isset($argument->value->value)) {
            throw new Exception('Could not determine value on scalar on end statement: ' . json_encode($argument, JSON_THROW_ON_ERROR));
        }

        return $argument->value->value;
    }
}
