<?php
declare(strict_types=1);

namespace PhpGraphClimber\Expression;

use Exception;
use PhpGraphClimber\Container\Container;
use PhpGraphClimber\ExpressionMap;
use PhpParser\Node;
use PhpParser\Node\Expr\MethodCall;

class AssignExpressionFactory
{
    public function createExpression(Node $node): AssignExpressionInterface
    {
        $container = Container::create();
        //$expressionHelper = $container->get(ExpressionHelper::class);
        //$branchHelper = $container->get(BranchHelper::class);

        $expressionName = self::determineExpressionName($node);

        switch ($expressionName)
        {
            case 'AssignVariableMethodCall';
                $assignVariableMethodCallExpression = new AssignVariableMethodCallExpression;
                $assignVariableMethodCallExpression->setNode($node);
                return $assignVariableMethodCallExpression;

            default:
                throw new Exception('Expression name not recognized');
        }

    }

    private static function determineExpressionName($node): string
    {
        $condition = $paramLeft = $paramRight = '';

        if (in_array($node->getType(), ExpressionMap::ASSIGN_EXPRESSIONS, true)) {
            $condition = 'Assign';

            if ($node->expr instanceof MethodCall) {
                $paramLeft = 'Variable';
                $paramRight = 'MethodCall';
            }
        }


        if ($paramLeft === '' || $paramRight === '') {
            throw new Exception('Operands are not recognized: ' . json_encode($node, JSON_THROW_ON_ERROR));
        }

        return $condition . $paramLeft . $paramRight;
    }
}