<?php
declare(strict_types=1);

namespace PhpGraphClimber\Expression;

use PhpGraphClimber\Variable\Variable;
use PhpParser\Node;
use PhpParser\Node\Expr\MethodCall;
use PhpParser\Node\Expr\Variable as NodeVariable;

interface AssignExpressionInterface
{
    public function setNode(Node $node): void;
    public function createVariable(NodeVariable $variable, MethodCall $methodCall): Variable;
}