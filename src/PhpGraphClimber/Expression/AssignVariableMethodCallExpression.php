<?php
declare(strict_types=1);

namespace PhpGraphClimber\Expression;

use PhpGraphClimber\Branch\BranchHelper;
use PhpGraphClimber\Variable\Variable;
use PhpGraphClimber\Variable\VariableValueMethodCall;
use PhpParser\Node;
use PhpParser\Node\Expr\MethodCall;
use PhpParser\Node\Expr\Variable as NodeVariable;

class AssignVariableMethodCallExpression implements AssignExpressionInterface
{
    /**
     * @var Node
     */
    private $node;

    public function __construct(
    )
    {
    }

    public function setNode(Node $node): void
    {
        $this->node = $node;
    }

    public function createVariable(NodeVariable $variable, MethodCall $methodCall): Variable
    {
        $variable = new Variable($variable->name);
        $variable->setValue(
            new VariableValueMethodCall(
                $methodCall->var->name,
                $methodCall->name->name,
                $methodCall->args ?? [],
            )
        );

        return $variable;
    }
}
