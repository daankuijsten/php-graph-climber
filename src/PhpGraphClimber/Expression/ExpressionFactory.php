<?php
declare(strict_types=1);

namespace PhpGraphClimber\Expression;

use Exception;
use PhpGraphClimber\Branch\BranchHelper;
use PhpGraphClimber\Container\Container;
use PhpGraphClimber\ExpressionMap;
use PhpParser\Node;
use PhpParser\Node\Expr\BinaryOp\Identical;
use PhpParser\Node\Expr\BinaryOp\NotIdentical;
use PhpParser\Node\Expr\Variable;
use PhpParser\Node\Scalar;

class ExpressionFactory
{
    public function createExpression(Node $node): ExpressionInterface
    {
        $container = Container::create();
        $expressionHelper = $container->get(ExpressionHelper::class);
        $branchHelper = $container->get(BranchHelper::class);

        $expressionName = self::determineExpressionName($node);

        switch ($expressionName)
        {
            case 'IdenticalScalarVariable';
                $identicalScalarVariableExpression = new IdenticalScalarVariableExpression(
                    $expressionHelper,
                    $branchHelper,
                );
                $identicalScalarVariableExpression->setNode($node);
                return $identicalScalarVariableExpression;

            case 'IdenticalScalarVariable';
                $identicalScalarVariableExpression = new IdenticalScalarVariableExpression(
                    $expressionHelper,
                    $branchHelper,
                );
                $identicalScalarVariableExpression->setNode($node);
                return $identicalScalarVariableExpression;

            default:
                throw new Exception('Expression name not recognized');
        }

    }

    private static function determineExpressionName($node): string
    {
        $condition = $paramLeft = $paramRight = '';

        if (in_array($node->cond->getType(), ExpressionMap::IDENTICAL_EXPRESSIONS, true)) {
            if ($node->cond instanceof Identical) {
                $condition = 'Identical';
            }

            if ($node->cond instanceof NotIdentical) {
                $condition = 'NotIdentical';
            }

            if ($node->cond->left instanceof Variable && $node->cond->right instanceof Variable) {
                $paramLeft = 'Variable';
                $paramRight = 'Variable';
            }

            if ($node->cond->left instanceof Variable && $node->cond->right instanceof Scalar) {
                $paramLeft = 'Variable';
                $paramRight = 'Scalar';
            }

            if ($node->cond->left instanceof Scalar && $node->cond->right instanceof Variable) {
                $paramLeft = 'Scalar';
                $paramRight = 'Variable';
            }
        }

        if (in_array($node->getType(), ExpressionMap::ASSIGN_EXPRESSIONS, true)) {



            $condition = 'Assign';

            $paramLeft = $node->var->name;
            $paramRight = $node->var->name;
            switch ($node->expr->getType()) {
                case LNumber::class:
                    $paramRight = $node->expr->value;
                    break;
                case Plus::class:
                    $paramRight = $node->expr->value;
                break;

            }

            $condition = 'Assign';

        }


        if ($paramLeft === '' || $paramRight === '') {
            throw new Exception('Operands are not recognized: ' . json_encode($node, JSON_THROW_ON_ERROR));
        }

        return $condition . $paramLeft . $paramRight;
    }

    /*
    private function resolveExpression($nodeLeft, $nodeRight, $nodeOperation) {

        if ($nodeLeft)


        switch ($nodeLeft->getType()) {
            case LNumber::class:
                $paramRight = $node->expr->value;
                break;
            case Plus::class:
                $paramRight = $node->expr->value;
                break;
        }

    }
    */
}