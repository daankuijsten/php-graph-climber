<?php
declare(strict_types=1);

namespace PhpGraphClimber\Expression;

use Exception;
use PhpGraphClimber\Operator\OperatorHelper;
use PhpGraphClimber\Parameter\Parameter;
use PhpGraphClimber\Parameter\ParameterGroup;
use PhpGraphClimber\Parameter\ParameterGroupCollection;
use PhpGraphClimber\Range\Range;
use RuntimeException;

class ExpressionHelper
{
    /**
     * @var OperatorHelper
     */
    private $operatorHelper;

    public function __construct(
        OperatorHelper $operatorHelper
    )
    {
        $this->operatorHelper = $operatorHelper;
    }

    public function getNodeNumbers(array $statements): array
    {
        $nodeNumbers = [];
        foreach ($statements as $statement) {
            $nodeNumbers[] = spl_object_id($statement);
        }
        return $nodeNumbers;
    }

    public function checkNode($node): void
    {
        if (!isset($node)) {
            throw new RuntimeException('Node is not set');
        }
    }

    public function checkCondition($condition): void
    {
        if (!isset($condition)) {
            throw new RuntimeException('Node does not have condition');
        }
    }

    public function checkIsObject($node): void
    {
        if (!is_object($node)) {
            throw new RuntimeException('Body is not an object: ' . json_encode($node, JSON_THROW_ON_ERROR));
        }
    }

    public function checkElseIfStatement($elseIfStatement): void
    {
        if (!empty($elseIfStatement)) {
            throw new Exception('The "elseif" is not implemented yet', 501);
        }
    }

    public function createParameterGroupCollectionFromValue(
        string $name,
        $value
    ): ParameterGroupCollection {
        $parameter = new Parameter($name);
        $parameter->setValue($value);
        $parameter->setOriginalValue($value);

        $parameter->addRange(new Range(
            'int',
            $value,
            $value
        ));

        $parameterGroup = new ParameterGroup();
        $parameterGroup->add($parameter);

        $parameterGroupCollection = new ParameterGroupCollection;
        $parameterGroupCollection->add($parameterGroup);

        return $parameterGroupCollection;
    }

    public function createParameterGroupCollectionFromNotValue(
        string $name,
        $value
    ): ParameterGroupCollection {
        $parameter = new Parameter($name);
        $parameter->setValue($this->operatorHelper->getNotValue($value));
        $parameter->setOriginalValue($value);

        foreach($this->operatorHelper->getRangeBoundariesForNotValue($value) as $rangeBoundaries) {
            $parameter->addRange(new Range(
                'int',
                $rangeBoundaries[0],
                $rangeBoundaries[1],
            ));
        }

        $parameterGroup = new ParameterGroup();
        $parameterGroup->add($parameter);

        $parameterGroupCollection = new ParameterGroupCollection;
        $parameterGroupCollection->add($parameterGroup);

        return $parameterGroupCollection;


    }

}
