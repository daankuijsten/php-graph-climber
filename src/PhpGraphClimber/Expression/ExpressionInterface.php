<?php
declare(strict_types=1);

namespace PhpGraphClimber\Expression;

use PhpParser\Node;

interface ExpressionInterface
{
    public function setNode(Node $node): void;
    public function createBranches(int $nodeNumber, ?array $nodeNumbersParentBranch): array;
}