<?php
declare(strict_types=1);

namespace PhpGraphClimber\Expression;

use PhpGraphClimber\Branch\BranchHelper;
use PhpParser\Node;

class NotIdenticalVariableScalarExpression implements ExpressionInterface
{
    /**
     * @var Node
     */
    private $node;

    /**
     * @var ExpressionHelper
     */
    private $expressionHelper;

    /**
     * @var BranchHelper
     */
    private $branchHelper;

    public function __construct(
        ExpressionHelper $expressionHelper,
        BranchHelper $branchHelper
    )
    {
        $this->expressionHelper = $expressionHelper;
        $this->branchHelper = $branchHelper;
    }

    public function setNode(Node $node): void
    {
        $this->node = $node;
    }

    public function createBranches(int $nodeNumber, ?array $nodeNumbersParentBranch): array
    {
        $this->expressionHelper->checkNode($this->node);
        $this->expressionHelper->checkCondition($this->node->cond);


        $ifBodyNode = $this->node->stmts[0];
        $this->expressionHelper->checkIsObject($ifBodyNode);

        $ifBodyNodeNumbers = $this->expressionHelper->getNodeNumbers($this->node->stmts);
        $elseBodyNodeNumber = null;

        if (isset($this->node->else, $this->node->else->stmts[0])) {
            $elseBodyNodeNumber = $this->expressionHelper->getNodeNumbers($this->node->else->stmts);
        }

        $this->expressionHelper->checkElseIfStatement($this->node->elseifs);

        $variable = $this->node->cond->left;
        $scalar = $this->node->cond->right;

        $parameterGroupCollection1 = $this->expressionHelper->createParameterGroupCollectionFromNotValue(
            $variable->name,
            $scalar->value
        );

        $branches = [
            $this->branchHelper->nodeNumbersToBranch(
                $ifBodyNodeNumbers,
                $nodeNumbersParentBranch,
                $parameterGroupCollection1,
                $nodeNumber
            )
        ];

        if (isset($elseBodyNodeNumber)) {
            $parameterGroupCollection2 = $this->expressionHelper->createParameterGroupCollectionFromValue(
                $variable->name,
                $scalar->value
            );

            $branches[] = $this->branchHelper->nodeNumbersToBranch(
                $elseBodyNodeNumber,
                $nodeNumbersParentBranch,
                $parameterGroupCollection2,
                $nodeNumber
            );

        }

        return $branches;
    }
}
