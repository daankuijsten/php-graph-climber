<?php
declare(strict_types=1);

namespace PhpGraphClimber;

class ExpressionMap
{
    public const IDENTICAL_EXPRESSIONS = [
        'Expr_BinaryOp_Identical',
        'Expr_BinaryOp_NotIdentical',
    ];

    public const STOP_EXECUTION_STATEMENTS = [
        'Expr_Exit',
        'Stmt_Throw',
        'Stmt_Return',
    ];

    public const BRANCH_EXPRESSIONS = [
        'Stmt_If',
    ];

    public const ASSIGN_EXPRESSIONS = [
        'Expr_Assign',
    ];

    public const FUNCTION_CALL_EXPRESSIONS = [
        'Expr_StaticCall',
    ];

}
