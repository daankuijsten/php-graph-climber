<?php
declare(strict_types=1);

namespace PhpGraphClimber\FileHelper;

use Exception;
use Nette\PhpGenerator\PhpFile;

class GeneralFileHelper
{
    private $writeDirectory = __DIR__ . '/../../../';
    private $writeFileExtension = '.php';

    /**
     * @var PhpFileHelper
     */
    private $phpFileHelper;

    public function __construct(PhpFileHelper $phpFileHelper)
    {
        $this->phpFileHelper = $phpFileHelper;
    }

    public function write(PhpFile $phpFile, string $content)
    {
        $phpNamespaceName = $this->phpFileHelper->getNamespaceName($phpFile);
        $className = $this->phpFileHelper->getClassName($phpFile);

        $filePathNamespace = str_replace('\\', DIRECTORY_SEPARATOR, $phpNamespaceName);
        $filePath = $this->writeDirectory . DIRECTORY_SEPARATOR . $filePathNamespace;
        $fileName = $className. $this->writeFileExtension;

        if (!is_dir($filePath) && !mkdir($concurrentDirectory = $filePath, 0755, true) && !is_dir($concurrentDirectory)) {
            throw new Exception(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }

        return file_put_contents($filePath . DIRECTORY_SEPARATOR . $fileName , $content);
    }
}
