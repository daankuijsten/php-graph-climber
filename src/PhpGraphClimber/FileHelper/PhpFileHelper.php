<?php
declare(strict_types=1);

namespace PhpGraphClimber\FileHelper;

use Exception;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PhpNamespace;

class PhpFileHelper
{
    public function getNamespaceName(PhpFile $phpFile): string
    {
        $namespaces = $phpFile->getNamespaces();

        if (count($namespaces) !== 1) {
            throw new Exception('There should be exactly 1 namespace in 1 file: "' . json_encode($namespaces, JSON_THROW_ON_ERROR) . '"');
        }

        $phpNamespace = array_pop($namespaces);
        $namespaceName = $phpNamespace->getName();
        if (!isset($namespaceName)) {
            throw new Exception('Could not find namespace');
        }

        return $namespaceName;
    }

    public function getClassName(PhpFile $phpFile): string
    {
        $classes = $phpFile->getClasses();

        if (count($classes) !== 1) {
            throw new Exception('There should be exactly 1 class in 1 file: "' . json_encode($classes, JSON_THROW_ON_ERROR) . '"');
        }

        $classType = array_pop($classes);
        $className = $classType->getName();
        if (!isset($className)) {
            throw new Exception('Could not find className');
        }

        return $className;
    }

    public function getNamespace(PhpFile $phpFile): PhpNamespace
    {
        $namespaces = $phpFile->getNamespaces();

        if (count($namespaces) !== 1) {
            throw new Exception('There should be exactly 1 namespace in 1 file: "' . json_encode($namespaces, JSON_THROW_ON_ERROR) . '"');
        }

        return array_pop($namespaces);
    }

    public function getClass(PhpNamespace $phpNamespace): ClassType
    {
        $classes = $phpNamespace->getClasses();

        if (count($classes) !== 1) {
            throw new Exception('There should be exactly 1 class in 1 file: "' . json_encode($classes, JSON_THROW_ON_ERROR) . '"');
        }

        return array_pop($classes);
    }

    public function createTestClassName(string $className, string $classNameSuffix): string
    {
        return $className . $classNameSuffix;
    }

    public function getFQDN(PhpFile $phpFile): string
    {
        $namespaceName = $this->getNamespaceName($phpFile);
        $className = $this->getClassName($phpFile);

        return $namespaceName . '\\' . $className;
    }

    public function fqdnToClassName(string $fqdn): string
    {
        return substr(strrchr($fqdn, '\\'), 1);
    }

    public function classNameToPropertyName(string $className): string
    {
        return lcfirst($className);

    }

}
