<?php
declare(strict_types=1);

namespace PhpGraphClimber;

use Exception;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PsrPrinter;
use PhpGraphClimber\Branch\BranchHelper;
use PhpGraphClimber\FileHelper\GeneralFileHelper;
use PhpGraphClimber\FileHelper\PhpFileHelper;
use PhpGraphClimber\Generator\FileGenerator;
use PhpGraphClimber\Container\Container;
use PhpGraphClimber\Generator\ClassGenerator;
use PhpGraphClimber\Graph\NodeToClassMethodGraphConverter;
use PhpGraphClimber\Generator\MethodGenerator\MethodGenerator;
use PhpGraphClimber\NodeParser\NodeParser;
use PhpGraphClimber\NodeParser\NodeParserHelper;
use PhpGraphClimber\PossibleInput\PossibleInputCalculator;
use PhpGraphClimber\TestFunctionBlueprint\TestFunctionBlueprintCreator;
use PhpParser\Error;
use PhpParser\ParserFactory;

class Generate
{

    public function __construct() {
        set_error_handler([$this, "warningHandler"], E_WARNING);
    }

    public function warningHandler($errorNumber, $errorString): void
    {
        throw new Exception($errorString);
    }

    public function run(string $filePath): void
    {
        try {
            $container = Container::create();

            $fileOriginal = file_get_contents($filePath);
            $parser = (new ParserFactory)->create(ParserFactory::PREFER_PHP7);

            $phpFileOriginal = PhpFile::fromCode($fileOriginal);

            $statements = $parser->parse($fileOriginal);

            /** @var NodeParser $nodeParser */
            $nodeParser = $container->get(NodeParser::class);
            /** @var NodeParserHelper $nodeParserHelper */
            $nodeParserHelper = $container->get(NodeParserHelper::class);
            /** @var MethodGenerator $methodGenerator */
            $methodGenerator = $container->get(MethodGenerator::class);
            /** @var ClassGenerator $classGenerator */
            $classGenerator = $container->get(ClassGenerator::class);
            /** @var NodeToClassMethodGraphConverter $nodeToClassMethodGraphConverter */
            $nodeToClassMethodGraphConverter = $container->get(NodeToClassMethodGraphConverter::class);
            /** @var PhpFileHelper $phpFileHelper */
            $phpFileHelper = $container->get(PhpFileHelper::class);
            /** @var PossibleInputCalculator $possibleValueCalculator */
            $possibleValueCalculator = $container->get(PossibleInputCalculator::class);
            /** @var TestFunctionBlueprintCreator $testFunctionBlueprintCreator */
            $testFunctionBlueprintCreator = $container->get(TestFunctionBlueprintCreator::class);
            /** @var BranchHelper $branchHelper */
            $branchHelper = $container->get(BranchHelper::class);

            $phpNamespace = $classGenerator->createNamespaceAndClass($phpFileOriginal);

            // Find all class methods
            $class = $phpFileHelper->getClass($phpFileHelper->getNamespace($phpFileOriginal));
            $classMethods = $class->getMethods();

            foreach ($classMethods as $classMethod) {
                // TODO: skip constructor
                if ($classMethod->getName() === '__construct') {
                    continue;
                }

                $classMethodGraph = $nodeToClassMethodGraphConverter::nodeToClassMethodGraph($class, $classMethod);

                $nodeClassMethod = $nodeParserHelper->findFirstClassMethodByName($statements, $classMethod->getName());
                if (is_null($nodeClassMethod)) {
                    throw new Exception('Could not find node class method');
                }
                // TODO, still needed with reverse traverse?
                $branch = $branchHelper->firstNodeToBranch($classMethod);
                $classMethodGraph->setBranches([$branch]);
                $classMethodGraph->pushBranchStack($branch->getNodeNumbers());

                $nodeParser->parse($nodeClassMethod, $classMethodGraph);

                $possibleValueCalculator->calculatePossibleInputValues($classMethodGraph);

                +d($classMethodGraph);
                die;
                $testFunctionBlueprints = $testFunctionBlueprintCreator->createTestFunctionBlueprints($classMethodGraph);

                foreach($testFunctionBlueprints as $testFunctionBlueprint) {
                    $methodGenerator->addTestFunctionToClass($phpNamespace, $testFunctionBlueprint);
                }
            }

            /** @var FileGenerator $fileGenerator */
            $fileGenerator = $container->get(FileGenerator::class);
            $phpFileOriginal = $fileGenerator->createFile($phpNamespace);

            /** @var GeneralFileHelper $fileHelper */
            $fileHelper = $container->get(GeneralFileHelper::class);
            $fileHelper->write($phpFileOriginal, (new PsrPrinter)->printFile($phpFileOriginal));
            echo 'Successfully written file!';
        } catch (Error $e) {
            echo 'Error: ', $e->getMessage();
        }
    }
}
