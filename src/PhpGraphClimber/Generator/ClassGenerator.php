<?php
declare(strict_types=1);

namespace PhpGraphClimber\Generator;

use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PhpNamespace;
use PhpGraphClimber\FileHelper\PhpFileHelper;
use PhpGraphClimber\Generator\MethodGenerator\MethodGenerator;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

class ClassGenerator
{
    private $classNameSuffix = 'Test';
    private $extendsFrom = TestCase::class;
    private $classInSetup = true;
    private $namespacePrefix = 'generated';
    private $trait = ProphecyTrait::class;

    /**
     * @var PhpFileHelper
     */
    private $phpFileHelper;

    /**
     * @var MethodGenerator
     */
    private $methodGenerator;

    public function __construct(PhpFileHelper $phpFileHelper, MethodGenerator $methodGenerator)
    {
        $this->phpFileHelper = $phpFileHelper;
        $this->methodGenerator = $methodGenerator;
    }

    public function createNamespaceAndClass(PhpFile $phpFile): PhpNamespace
    {
        $namespaceName = $this->phpFileHelper->getNamespaceName($phpFile);
        $phpNamespace = new PhpNamespace($this->namespacePrefix . '\\' . $namespaceName);
        $className = $this->phpFileHelper->getClassName($phpFile);
        $classType = new ClassType($this->phpFileHelper->createTestClassName($className, $this->classNameSuffix));

        $classType
            ->setFinal()
            ->setExtends($this->extendsFrom);

        $phpNamespace->add($classType);
        $phpNamespace->addUse($this->extendsFrom);

        // TODO: make configurable
        $phpNamespace->addUse($this->trait);
        $classType->addTrait($this->trait);

        if ($this->classInSetup) {
            $this->createClassInSetUpFunction($phpNamespace, $phpFile);
        }

        return $phpNamespace;
    }

    private function createClassInSetUpFunction(PhpNamespace $phpNamespace, PhpFile $phpFile): void
    {
        //$fqdn = $this->phpFileHelper->getFQDN($phpFile);
        $this->methodGenerator->createSetUpMethodWithTestClass($phpNamespace, $phpFile);
    }

}