<?php
declare(strict_types=1);

namespace PhpGraphClimber\Generator;

use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PhpNamespace;

class FileGenerator
{
    private $setStrictTypes = true;

    public function createFile(PhpNamespace $phpNamespace): PhpFile
    {
        $file = new PhpFile;
        if ($this->setStrictTypes) {
            $file->setStrictTypes();
        }

        $file->addNamespace($phpNamespace);
        return $file;
    }
}