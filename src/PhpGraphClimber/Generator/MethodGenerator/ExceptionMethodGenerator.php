<?php
declare(strict_types=1);

namespace PhpGraphClimber\Generator\MethodGenerator;

use PhpGraphClimber\EndStatement\EndStatement;
use PhpGraphClimber\EndStatement\ThrowEndStatement;
use PhpGraphClimber\NameMap;
use PhpGraphClimber\TestFunctionBlueprint\TestFunctionBlueprint;

class ExceptionMethodGenerator implements MethodGeneratorInterface
{
    public function generateMethod(
        string $methodCall,
        TestFunctionBlueprint $testFunctionBlueprint
    ): string
    {
        $return = '$this->' .
            $this->paramToTestFunctionAssertStatement(
                $testFunctionBlueprint->getEndStatement(),
                ThrowEndStatement::TYPE,
            ) . '(\'' . $testFunctionBlueprint->getEndStatement()->getValue()->getType() . '\');';

        if ($testFunctionBlueprint->getEndStatement()->getValue()->getMessage()) {
            $return .= "\n" . '$this->' .
                $this->paramToTestFunctionAssertStatement(
                    $testFunctionBlueprint->getEndStatement(),
                    ThrowEndStatement::MESSAGE,
                ) . '(\'' . $testFunctionBlueprint->getEndStatement()->getValue()->getMessage() . '\');';

        }

        if ($testFunctionBlueprint->getEndStatement()->getValue()->getCode()) {
            $return .= "\n" . '$this->' .
                $this->paramToTestFunctionAssertStatement(
                    $testFunctionBlueprint->getEndStatement(),
                    ThrowEndStatement::CODE,
                ) . '(' . $testFunctionBlueprint->getEndStatement()->getValue()->getCode() . ');';

        }

        $return .= "\n\n" . $methodCall . ';';
        return $return;
    }

    public function paramToTestFunctionShould(EndStatement $endStatement): string
    {
        return NameMap::ASSERTS[$endStatement->getType()]['testFunctionShould'] . $endStatement->getValue()->getType();
    }

    private function paramToTestFunctionAssertStatement(EndStatement $endStatement, string $exceptionParameter): string
    {
        return NameMap::ASSERTS[$endStatement->getType()]['testFunctionAssertStatement'][$exceptionParameter];
    }

}