<?php
declare(strict_types=1);

namespace PhpGraphClimber\Generator\MethodGenerator;

use Exception;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\Method;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PhpNamespace;
use PhpGraphClimber\FileHelper\PhpFileHelper;
use PhpGraphClimber\Operator\OperatorHelper;
use PhpGraphClimber\ResultInputValue\ResultInputValue;
use PhpGraphClimber\TestFunctionBlueprint\TestFunctionBlueprint;
use Prophecy\Prophecy\ObjectProphecy;

class MethodGenerator
{
    private $methodNamePrefix = 'test';
    private $mockClass = ObjectProphecy::class;

    private const MOCK_PARAMETER_TYPE_DEFAULT = [
        'string' => '\'foo\'',
        'int' => '1',
        'float' => '1.0',
        'bool' => 'true',
        'array' => '[]',
        'iterable' => '[]',
        'callable' => 'function() {}',
        'object' => '(new \stdClass)',
        'self' => '(new \stdClass)',
        'null' => '\'foo\'',
    ];

    /**
     * @var OperatorHelper
     */
    private $operatorHelper;

    /**
     * @var PhpFileHelper
     */
    private $phpFileHelper;

    /**
     * @var MethodGeneratorFactory
     */
    private $methodGeneratorFactory;

    public function __construct(
        OperatorHelper $operatorHelper,
        PhpFileHelper $phpFileHelper,
        MethodGeneratorFactory $methodGeneratorFactory
    )
    {
        $this->operatorHelper = $operatorHelper;
        $this->phpFileHelper = $phpFileHelper;
        $this->methodGeneratorFactory = $methodGeneratorFactory;
    }

    public function addTestFunctionToClass(PhpNamespace $phpNamespace, TestFunctionBlueprint $testFunctionBlueprint): void
    {
        $methodGeneratorInstance = $this->methodGeneratorFactory->createMethodGenerator(
            $testFunctionBlueprint->getEndStatement()
        );

        $methodName = [
            $this->methodNamePrefix,
            $testFunctionBlueprint->getFunctionName(),
            $methodGeneratorInstance->paramToTestFunctionGiven($testFunctionBlueprint->getPossibleInputParametersCollection()),
            $methodGeneratorInstance->paramToTestFunctionShould($testFunctionBlueprint->getEndStatement(), $testFunctionBlueprint->getPossibleInputParametersCollection())
        ];

        $methodBody = $this->createMethodBody($testFunctionBlueprint, $methodGeneratorInstance);

        $class = $this->phpFileHelper->getClass($phpNamespace);
        $class->addMethod(implode('_', $methodName))
            ->setPublic()
            ->setReturnType('void')
            ->setBody($methodBody);

        $phpNamespace->add($class);
    }

    private function createMethodBody(TestFunctionBlueprint $testFunctionBlueprint, MethodGeneratorInterface $methodGeneratorInstance): string
    {
        $propertyName = $this->phpFileHelper->classNameToPropertyName($testFunctionBlueprint->getClassName());
        return $methodGeneratorInstance->generateMethod($propertyName, $testFunctionBlueprint);
    }

    public function createSetUpMethodWithTestClass(PhpNamespace $phpNamespace, PhpFile $phpFileOriginal): void
    {
        $classType = $this->phpFileHelper->getClass($phpNamespace);
        $method = $classType
            ->addMethod('setUp')
            ->setReturnType('void')
            ->setPublic();

        $method->setBody('parent::setUp();' . "\n");

        $fqdnOriginal = $this->phpFileHelper->getFQDN($phpFileOriginal);
        $classTypeOriginal = $this->phpFileHelper->getClass($this->phpFileHelper->getNamespace($phpFileOriginal));
        $className = $classTypeOriginal->getName();
        $propertyName = $this->phpFileHelper->classNameToPropertyName($className);

        $phpNamespace->addUse($fqdnOriginal);

        $classType
            ->addProperty($propertyName)
            ->setPrivate()
            ->addComment('@var ' . $className . "\n");

        $constructorMethod = $this->mockConstructorMethodParameters($phpNamespace, $classTypeOriginal);

        $constructorMethodDefaultValues = $this->createConstructorMethodDefaultValues($constructorMethod);
        if ('' !== $constructorMethodDefaultValues) {
            $constructorMethodDefaultValues = "(\n" . $constructorMethodDefaultValues . "\n)";
        }

        $method->addBody("\n" . '$this->' . $propertyName . ' = new ' . $className . $constructorMethodDefaultValues . ';');
    }

    private function createConstructorMethodDefaultValues(?Method $method): string
    {
        if (is_null($method)) {
            return '';
        }

        $defaultParameters = [];
        $indent = '    ';

        foreach ($method->getParameters() as $parameter) {
            $parameterType = $parameter->getType();

            if (is_null($parameterType)) {
                $defaultParameters[] = $indent . self::MOCK_PARAMETER_TYPE_DEFAULT['null'];
                continue;
            }

            if (class_exists($parameterType)) {
                $className = $this->phpFileHelper->fqdnToClassName($parameterType);
                $propertyName = $this->phpFileHelper->classNameToPropertyName($className);
                $defaultParameters[] = $indent . '$this->' . $propertyName . '->reveal()';

                continue;
            }

           $defaultParameters[] = $indent . self::MOCK_PARAMETER_TYPE_DEFAULT[$parameterType];
        }

        return implode(",\n", $defaultParameters);
    }

    private function mockConstructorMethodParameters(PhpNamespace $phpNamespace, ClassType $classTypeOriginal): ?Method
    {
        $fqdnClassNameList = [];
        $constructorMethod = $classTypeOriginal->getMethods()['__construct'] ?? null;

        if (is_null($constructorMethod)) {
            return null;
        }

        foreach ($constructorMethod->getParameters() as $parameter) {
            $parameterType = $parameter->getType();

            if (is_null($parameterType)) {
                continue;
            }

            if (class_exists($parameterType)) {
                $fqdnClassNameList[] = $parameterType;
                continue;
            }

            if (!array_key_exists($parameterType, self::MOCK_PARAMETER_TYPE_DEFAULT)) {
                throw new Exception('Parameter type not yet supported: "' . $parameterType . '"');
            }
        }

        $this->addMocksToSetUpMethod($phpNamespace, $fqdnClassNameList);

        return $constructorMethod;
    }

    private function addMocksToSetUpMethod(PhpNamespace $phpNamespace, array $fqdnClassNameList): void
    {
        $classType = $this->phpFileHelper->getClass($phpNamespace);
        $method = $classType
            ->getMethod('setUp');

        foreach ($fqdnClassNameList as $fqdnClassName) {
            $className = $this->phpFileHelper->fqdnToClassName($fqdnClassName);
            $classNameMock = $this->phpFileHelper->fqdnToClassName($this->mockClass);
            $propertyName = $this->phpFileHelper->classNameToPropertyName($className);

            $phpNamespace->addUse($this->mockClass);
            $phpNamespace->addUse($fqdnClassName);

            $classType
                ->addProperty($propertyName)
                ->setPrivate()
                ->addComment('@var ' . $classNameMock . '|' . $className . "\n");

            $mock = '$this->prophesize(' . $className . '::class);';

            $method->addBody('$this->' . $propertyName . ' = ' . $mock);
        }
    }

}