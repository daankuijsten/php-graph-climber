<?php
declare(strict_types=1);

namespace PhpGraphClimber\Generator\MethodGenerator;

use Exception;
use PhpGraphClimber\EndStatement\EndStatement;
use PhpGraphClimber\Generator\MethodGenerator\Return_\ReturnMethodGeneratorBool;
use PhpGraphClimber\Generator\MethodGenerator\Return_\ReturnMethodGeneratorInt;
use PhpGraphClimber\Generator\MethodGenerator\Return_\ReturnMethodGeneratorNull;
use PhpGraphClimber\PossibleInput\PossibleInputParameterInterface;
use PhpGraphClimber\PossibleInput\PossibleInputParameters;
use PhpGraphClimber\PossibleInput\PossibleInputParametersCollection;

class MethodGeneratorFactory
{
    /**
     * @var MethodGeneratorHelper
     */
    private $methodGeneratorHelper;

    public function __construct(MethodGeneratorHelper $methodGeneratorHelper)
    {
        $this->methodGeneratorHelper = $methodGeneratorHelper;
    }

    public function createMethodGenerator(
        EndStatement $endStatement
    ): MethodGeneratorInterface
    {
        switch ($endStatement->getType()) {
            case 'return';
                return $this->createReturnMethodGenerator($endStatement);

            case 'exception';
                return $this->createExceptionMethodGenerator($endStatement);

            default:
                throw new Exception('Method generator type not recognized');
        }
    }

    private function createReturnMethodGenerator(EndStatement $endStatement): MethodGeneratorInterface
    {
        switch ($endStatement->getValueType()) {
            case PossibleInputParameters::TYPE__INT:
                return new ReturnMethodGeneratorInt($this->methodGeneratorHelper);
//            case PossibleInputParameters::TYPE__FLOAT:
//                return new ReturnMethodGeneratorFloat($this->methodGeneratorHelper);
            case PossibleInputParameters::TYPE__BOOL:
                return new ReturnMethodGeneratorBool($this->methodGeneratorHelper);
//            case PossibleInputParameters::TYPE__OBJECT:
//                return new ReturnMethodGeneratorObject($this->methodGeneratorHelper);
//            case PossibleInputParameters::TYPE__STRING:
//                return new ReturnMethodGeneratorString($this->methodGeneratorHelper);
            case PossibleInputParameters::TYPE__NULL:
                return new ReturnMethodGeneratorNull($this->methodGeneratorHelper);
//            case PossibleInputParameters::TYPE__ARRAY:
//                return new ReturnMethodGeneratorArray($this->methodGeneratorHelper);
            default:
                throw new Exception('Method generator type not recognized');
        }
    }

    private function createExceptionMethodGenerator(PossibleInputParameterInterface $possibleInputParameter): MethodGeneratorInterface
    {
        return new ExceptionMethodGenerator();
    }

}
