<?php
declare(strict_types=1);

namespace PhpGraphClimber\Generator\MethodGenerator;

use Exception;

class MethodGeneratorHelper
{
    public function valueTypeToInputString($value): string
    {
        if (is_int($value)) {
            return (string) $value;
        }

        if (is_string($value)) {
            return "'$value'";
        }

        if (is_null($value)) {
            return 'null';
        }

        if (is_bool($value)) {
            return $value ? 'true' : 'false';
        }

        throw new Exception('Could not convert value type to input string: ' . @d($value));
    }
}