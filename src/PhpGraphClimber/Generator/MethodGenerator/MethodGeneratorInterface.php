<?php
declare(strict_types=1);

namespace PhpGraphClimber\Generator\MethodGenerator;

use PhpGraphClimber\EndStatement\EndStatement;
use PhpGraphClimber\TestFunctionBlueprint\TestFunctionBlueprint;

interface MethodGeneratorInterface
{
    public function generateMethod(string $propertyName, TestFunctionBlueprint $testFunctionBlueprint): string;
    public function paramToTestFunctionShould(EndStatement $endStatement): string;
    public function paramToTestFunctionAssertStatement(EndStatement $endStatement): string;

}
