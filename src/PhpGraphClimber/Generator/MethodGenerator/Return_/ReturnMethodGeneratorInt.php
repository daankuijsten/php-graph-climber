<?php
declare(strict_types=1);

namespace PhpGraphClimber\Generator\MethodGenerator\Return_;

use PhpGraphClimber\EndStatement\EndStatement;
use PhpGraphClimber\Generator\MethodGenerator\MethodGeneratorHelper;
use PhpGraphClimber\Generator\MethodGenerator\MethodGeneratorInterface;
use PhpGraphClimber\NameMap;
use PhpGraphClimber\PossibleInput\PossibleInputParametersCollection;
use PhpGraphClimber\TestFunctionBlueprint\TestFunctionBlueprint;

class ReturnMethodGeneratorInt implements MethodGeneratorInterface
{
    /**
     * @var MethodGeneratorHelper
     */
    private $methodGeneratorHelper;

    public function __construct(MethodGeneratorHelper $methodGeneratorHelper)
    {
        $this->methodGeneratorHelper = $methodGeneratorHelper;
    }

    public function generateMethod(string $propertyName, TestFunctionBlueprint $testFunctionBlueprint): string
    {
        $endStatement = $testFunctionBlueprint->getEndStatement();
        $functionAssertStatement = $this->paramToTestFunctionAssertStatement($endStatement);
        $methodCall = $this->generateMethodCall($propertyName, $testFunctionBlueprint);

        $returnValue = $this->methodGeneratorHelper->valueTypeToInputString($endStatement->getValue());
        return '$this->' . $functionAssertStatement . '(' . $returnValue . ', ' . $methodCall . ');';
    }

    private function generateMethodCall(
        string $propertyName,
        TestFunctionBlueprint $testFunctionBlueprint
    ): string
    {
        $inputStrings = [];

        $possibleInputParametersCollection = $testFunctionBlueprint->getPossibleInputParametersCollection();
        foreach ($possibleInputParametersCollection->getAll() as $possibleInputParameters) {
            $variableName = $possibleInputParameters->getVariableName();
            if (is_null($variableName)) {
                continue;
            }

            foreach ($possibleInputParameters->getAll() as $possibleInputParameter) {
                $inputStrings[] = $this->methodGeneratorHelper->valueTypeToInputString($possibleInputParameter->getValueResult());
            }
        }

        return '$this->' . $propertyName
            . '->' . $testFunctionBlueprint->getFunctionName()
            . '(' . implode(', ', $inputStrings) . ')';
    }

    public function paramToTestFunctionShould(EndStatement $endStatement): string
    {
        return NameMap::ASSERTS[$endStatement->getType()][$endStatement->getValueType()]['testFunctionShould'];
    }

    public function paramToTestFunctionAssertStatement(EndStatement $endStatement): string
    {
        return NameMap::ASSERTS[$endStatement->getType()][$endStatement->getValueType()]['testFunctionAssertStatement'];
    }

    public function paramToTestFunctionGiven(PossibleInputParametersCollection $possibleInputParametersCollection): string
    {
        $given = [];
        foreach ($possibleInputParametersCollection->getAll() as $possibleInputParameters) {
            // When there is no variable name, there was no input
            // TODO
            $variableName = $possibleInputParameters->getVariableName();
            if (is_null($variableName)) {
                continue;
            }

            $given[] = ucfirst($variableName)
                . ucfirst($this->methodGeneratorHelper->valueTypeToInputString($possibleInputParameters->getAll()[0]->getValueResult()));
        }

        if ($given === []) {
            $given[] = 'noInput';
        }

        return lcfirst(implode('And', $given));
    }

}