<?php
declare(strict_types=1);

namespace PhpGraphClimber\Graph;

use Exception;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\Method;
use PhpGraphClimber\Branch\Branch;

final class ClassMethodGraph
{
    /**
     * @var string
     */
    private $method;

    /**
     * @var array
     */
    private $branches = [];

    private $depthLevel = 0;

    /**
     * @var array
     */
    private $branchStack = [];

    /**
     * @var ClassType
     */
    private $class;

    public function __construct(
        ClassType $class,
        Method $method
    )
    {
        $this->method = $method;
        $this->class= $class;
    }

    public function getClass(): ClassType
    {
        return $this->class;
    }

    public function getMethod(): Method
    {
        return $this->method;
    }

    public function getBranches(): array
    {
        return $this->branches;
    }

    public function setBranches(array $branches): void
    {
        $this->branches = $branches;
    }

    public function getBranchByNodeNumber(?int $nodeNumber): ?Branch
    {
        return $this->findInBranchTree($nodeNumber, $this->branches);
    }

    private function findInBranchTree(?int $nodeNumber, array $branches)
    {
        foreach($branches as $branch) {
            $branchChildren = $branch->getBranches();
            if (!empty($branchChildren)) {
                $branchChild = $this->findInBranchTree($nodeNumber, $branch->getBranches());
                if (!is_null($branchChild)) {
                    return $branchChild;
                }
            }

            if (in_array($nodeNumber, $branch->getNodeNumbers(), true)) {
                return $branch;
            }
        }

        return null;
    }

    public function getBranchByNodeNumbers(?array $nodeNumbers): ?Branch
    {
        return $this->findInBranchTree2($nodeNumbers, $this->branches);
    }

    private function findInBranchTree2(?array $nodeNumbers, array $branches)
    {
        foreach($branches as $branch) {
            $branchChildren = $branch->getBranches();
            if (!empty($branchChildren)) {
                $branchChild = $this->findInBranchTree2($nodeNumbers, $branchChildren);
                if (!is_null($branchChild)) {
                    return $branchChild;
                }
            }

            if ($nodeNumbers === $branch->getNodeNumbers()) {
                return $branch;
            }
        }

        return null;
    }

    public function addDepthLevel(): void
    {
        $this->depthLevel++;
    }

    public function removeDepthLevel(): void
    {
        $this->depthLevel--;

        if ($this->depthLevel < 0) {
            throw new Exception('DepthLevel reached a value < 0, this cannot happen');
        }
    }

    public function pushBranchStack(?array $numbers): void
    {
        $this->branchStack[] = $numbers;
    }

    public function popBranchStack(): void
    {
        array_pop($this->branchStack);
    }

    public function topBranchStack(): ?array
    {
        if (empty($this->branchStack)) {
            return null;
        }

        $array = array_values($this->branchStack);
        return end($array);
    }

    public function isInBranchStack(array $nodeNumbers): bool
    {
        return in_array($nodeNumbers, $this->branchStack, true);
    }

    public function getParentBranch(Branch $branch): ?Branch
    {
        return $this->getBranchByNodeNumbers($branch->getNodeNumbersParentBranch());
    }

    public function getBranchesByEndStatement(): array
    {
        return $this->findInBranchTreeByEndStatement($this->getBranches());
    }

    /**
     * @param Branch[] $branches
     */
    private function findInBranchTreeByEndStatement(array $branches): array
    {
        $results = [];
        foreach($branches as $branch) {
            $endStatement = $branch->getEndStatement();
            if (!is_null($endStatement)) {
                $results[] = $branch;
            }

            $branchChildren = $branch->getBranches();
            if (!empty($branchChildren)) {
                $branchChildResults = $this->findInBranchTreeByEndStatement($branchChildren);
                $results = array_merge($branchChildResults, $results);
            }
        }

        return $results;
    }

}
