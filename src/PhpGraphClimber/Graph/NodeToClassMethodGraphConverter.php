<?php
declare(strict_types=1);

namespace PhpGraphClimber\Graph;

use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\Method;

final class NodeToClassMethodGraphConverter
{
    public static function nodeToClassMethodGraph(ClassType $class, Method $method): ClassMethodGraph
    {
        return new ClassMethodGraph($class, $method);
    }
}
