<?php
declare(strict_types=1);

namespace PhpGraphClimber;

class NameMap
{
    public const ASSERTS = [
        'return' => [
            'true' => [
                'testFunctionShould' => 'returnTrue',
                'testFunctionAssertStatement' => 'assertTrue',
            ],
            'false' => [
                'testFunctionShould' => 'returnFalse',
                'testFunctionAssertStatement' => 'assertFalse',
            ],
            'null' => [
                'testFunctionShould' => 'returnNull',
                'testFunctionAssertStatement' => 'assertNull',
            ],
            'float' => [
                'testFunctionShould' => 'returnFloat',
                'testFunctionAssertStatement' => 'assertSame',
            ],
            'int' => [
                'testFunctionShould' => 'returnInt',
                'testFunctionAssertStatement' => 'assertSame',
            ],
            'string' => [
                'testFunctionShould' => 'returnString',
                'testFunctionAssertStatement' => 'assertSame',
            ],
            'array' => [
                'testFunctionShould' => 'returnArray',
                'testFunctionAssertStatement' => 'assertSame',
            ],
            'object' => [
                'testFunctionShould' => 'returnInstance',
                'testFunctionAssertStatement' => 'assertInstanceOf',
            ]
        ],
        'exception' => [
            'testFunctionShould' => 'throw',
            'testFunctionAssertStatement' => [
                'type' => 'expectException',
                'message' => 'expectExceptionMessage',
                'code' => 'expectExceptionCode',
            ]
        ]
    ];


}
