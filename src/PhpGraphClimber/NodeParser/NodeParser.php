<?php
declare(strict_types=1);

namespace PhpGraphClimber\NodeParser;

use PhpGraphClimber\Graph\ClassMethodGraph;
use PhpGraphClimber\NodeVisitor\NodeVisitor;
use PhpParser\Node;
use PhpParser\NodeTraverser;

final class NodeParser
{
    /**
     * @var NodeParserHelper
     */
    private $nodeParserHelper;

    public function __construct(NodeParserHelper $nodeParserHelper)
    {
        $this->nodeParserHelper = $nodeParserHelper;
    }

    public function parse(Node $functionNode, ClassMethodGraph $classMethodGraph): ClassMethodGraph
    {
        $traverser = new NodeTraverser;
        $nodeVisitor = new NodeVisitor($classMethodGraph, $this->nodeParserHelper);
        $traverser->addVisitor($nodeVisitor);
        $traverser->traverse([$functionNode]);
        return $classMethodGraph;
    }

}
