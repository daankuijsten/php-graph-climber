<?php
declare(strict_types=1);

namespace PhpGraphClimber\NodeParser;

use PhpGraphClimber\EndStatement\EndStatementFactory;
use PhpGraphClimber\Expression\AssignExpressionFactory;
use PhpGraphClimber\Expression\ExpressionFactory;
use PhpGraphClimber\ExpressionMap;
use PhpGraphClimber\Graph\ClassMethodGraph;
use PhpParser\Node;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\NodeFinder;
use PhpParser\PrettyPrinter\Standard;

final class NodeParserHelper
{
    /**
     * @var ExpressionFactory
     */
    private $expressionFactory;

    /**
     * @var EndStatementFactory
     */
    private $endStatementFactory;

    /**
     * @var AssignExpressionFactory
     */
    private $assignExpressionFactory;

    public function __construct(
        ExpressionFactory $expressionFactory,
        AssignExpressionFactory $assignExpressionFactory,
        EndStatementFactory $endStatementFactory
    )
    {
        $this->expressionFactory = $expressionFactory;
        $this->endStatementFactory = $endStatementFactory;
        $this->assignExpressionFactory = $assignExpressionFactory;
    }

    public function getFirstLineAsString(Node $node): string
    {
        $prettyPrinter = new Standard;
        $print = $prettyPrinter->prettyPrint([$node]);
        return '(' . $node->getType() . ') ' . substr($print, 0);
    }

    public function handleEnterNode(ClassMethodGraph $classMethodGraph, Node $node): void
    {
        $nodeNumber = $this->getNodeNumber($node);

        //var_dump('enter ' . $nodeNumber . ': ' . $this->getFirstLineAsString($node));
        $branch = $classMethodGraph->getBranchByNodeNumber($nodeNumber);
        if (isset($branch) && !$classMethodGraph->isInBranchStack($branch->getNodeNumbers())) {
            //var_dump('push branch stack ' . json_encode($branch->getNodeNumbers()));
            $classMethodGraph->pushBranchStack($branch->getNodeNumbers());
        }

        if (in_array($node->getType(), ExpressionMap::BRANCH_EXPRESSIONS)) {
            $expression = $this->expressionFactory->createExpression($node);
            // TODO: check if we HAVE branches (body is not empty)
            $nodeNumbersParentBranch = $classMethodGraph->topBranchStack();
            $branches = $expression->createBranches($nodeNumber, $nodeNumbersParentBranch);
            $parentBranch = $classMethodGraph->getBranchByNodeNumbers($nodeNumbersParentBranch);
            //var_dump($nodeNumberParentBranch, $parentBranch);
            if (!isset($parentBranch)) {
                //var_dump('neee{');
                //var_dump($classMethodGraph->topBranchStack());
                $classMethodGraph->addBranches($branches);
            } else {
                $parentBranch->addBranches($branches);
            }

            return;
        }

        if (in_array($node->getType(), ExpressionMap::STOP_EXECUTION_STATEMENTS)) {
            $nodeNumbersParentBranch = $classMethodGraph->topBranchStack();
            $parentBranch = $classMethodGraph->getBranchByNodeNumbers($nodeNumbersParentBranch);

            $parentBranch->endBranch();

            $endStatementType = $this->endStatementFactory->createEndStatement($node);
            $endStatement = $endStatementType->createEndStatement();
            $parentBranch->setEndStatement($endStatement);

            return;
        }

        /*
        if (in_array($node->getType(), ExpressionMap::ASSIGN_EXPRESSIONS)) {
            $expression = $this->assignExpressionFactory->createExpression($node);
            $variable = $expression->createVariable($node->var, $node->expr);

            $nodeNumbersParentBranch = $classMethodGraph->topBranchStack();
            $parentBranch = $classMethodGraph->getBranchByNodeNumbers($nodeNumbersParentBranch);
            $parentBranch->addVariable($variable);
            return;
        }
        */

    }

    public function handleLeaveNode(ClassMethodGraph $classMethodGraph, Node $node): void
    {
        $nodeNumber = $this->getNodeNumber($node);

        //var_dump('exit ' . $nodeNumber . ': ' . $this->getFirstLineAsString($node));

        // Are we leaving a branch?
        $branch = $classMethodGraph->getBranchByNodeNumber($nodeNumber);
        if (isset($branch)) {
            $branch->countLeaveNode();
            if ($branch->allNodesLeft()) {
                $classMethodGraph->popBranchStack();
                //var_dump('pop branch stack ' . json_encode($branch->getNodeNumbers()));
            }

        }
    }


    public function findFirstClassMethodByName(?array $statements, string $name): ?Node
    {
        $nodeFinder = new NodeFinder;
        return $nodeFinder->findFirst($statements, function(Node $node) use ($name) {
            return $node instanceof ClassMethod && $node->name->name === $name;
        });
    }

    private function getNodeNumber(Node $node): int
    {
        return spl_object_id($node);
    }
}
