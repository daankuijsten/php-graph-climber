<?php
declare(strict_types=1);

namespace PhpGraphClimber\NodeVisitor;

use InvalidArgumentException;
use PhpGraphClimber\Graph\ClassMethodGraph;
use PhpGraphClimber\NodeParser\NodeParserHelper;
use PhpParser\Node;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\NodeVisitorAbstract;

final class NodeVisitor extends NodeVisitorAbstract
{
    /**
     * @var ClassMethodGraph
     */
    private $classMethodGraph;

    /**
     * @var NodeParserHelper
     */
    private $nodeParserHelper;

    public function __construct(ClassMethodGraph $classMethodGraph, NodeParserHelper $nodeParserHelper)
    {
        $this->classMethodGraph = $classMethodGraph;
        $this->nodeParserHelper = $nodeParserHelper;
    }

    public function beforeTraverse(array $nodes): void
    {
        // Force processing only 1 node
        if (count($nodes) !== 1) {
            throw new InvalidArgumentException('Pass only 1 node to ' . __METHOD__);
        }

        $node = array_pop($nodes);

        if (! self::isClassMethod($node)) {
            throw new InvalidArgumentException(get_class($node->name) . ' passed to ' . __METHOD__);
        }
    }

    public function enterNode(Node $node): void
    {
        $this->nodeParserHelper->handleEnterNode($this->classMethodGraph, $node);
    }

    public function leaveNode(Node $node): void
    {
        $this->nodeParserHelper->handleLeaveNode($this->classMethodGraph, $node);
    }

    private static function isClassMethod(Node $node): bool
    {
        return $node instanceof ClassMethod;
    }
}
