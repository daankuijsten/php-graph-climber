<?php
declare(strict_types=1);

namespace PhpGraphClimber\Operator;

use Exception;
use PhpGraphClimber\EndStatement\EndStatement;
use PhpGraphClimber\NameMap;

class OperatorHelper
{
    public function getInverseOperator(string $operator): string
    {
        switch ($operator) {
            case 'Expr_BinaryOp_NotIdentical':
                return 'Expr_BinaryOp_Identical';
            case 'Expr_BinaryOp_Identical':
                return 'Expr_BinaryOp_NotIdentical';
            default:
                throw new Exception('Could not determine inverse operator of: "' . $operator . '"');
        }
    }

    public function paramToTestFunctionGiven(array $resultInputValues): string
    {
        $given = '';
        foreach ($resultInputValues as $resultInputValue) {
            $given .= $resultInputValue->getName() . $resultInputValue->getValue();
        }
        return $given;
    }

    public function getNotValue($value)
    {
        if (is_int($value)) {
            return ($value + 1);
        }

        if (is_float($value)) {
            return ($value + 1.0);
        }

        if (is_string($value)) {
            return ($value . 'foo');
        }

        if (is_bool($value)) {
            return !$value;
        }

        if (is_array($value)) {
            $value[] = 'foo';
            return $value;
        }

        if (is_null($value)) {
            return true;
        }

        if (is_object($value)) {
            return false;
        }

        if (is_callable($value)) {
            return false;
        }

        throw new Exception('Could not determine inverse value of: "' . json_encode($value, JSON_THROW_ON_ERROR) . '"');
    }

    public function getRangeBoundariesForNotValue($value): ?array
    {
        if (is_int($value)) {
            return [
                [PHP_INT_MIN, $value - 1],
                [($value + 1), PHP_INT_MAX]
            ];
        }

        throw new Exception('Could not determine inverse range value of: "' . json_encode($value, JSON_THROW_ON_ERROR) . '"');
    }

}
