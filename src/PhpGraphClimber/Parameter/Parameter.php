<?php
declare(strict_types=1);

namespace PhpGraphClimber\Parameter;

use PhpGraphClimber\Range\Range;

class Parameter
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var mixed
     */
    private $value;

    /**
     * @var mixed
     */
    private $originalValue;

    /**
     * @var array
     */
    private $ranges = [];

    public function __construct(
        string $name
    )
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value): void
    {
        $this->value = $value;
    }

    public function addRange(Range $range): void
    {
        $this->ranges[] = $range;
    }

    public function getRanges(): array
    {
        return $this->ranges;
    }

    public function getOriginalValue()
    {
        return $this->originalValue;
    }

    public function setOriginalValue($originalValue): void
    {
        $this->originalValue = $originalValue;
    }
}
