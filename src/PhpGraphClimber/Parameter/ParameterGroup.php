<?php
declare(strict_types=1);

namespace PhpGraphClimber\Parameter;

class ParameterGroup
{
    private $parameters = [];

    public function add(Parameter $parameter): void
    {
        $this->parameters[] = $parameter;
    }

    /**
     * @return Parameter[]
     */
    public function getAll(): array
    {
        return $this->parameters;
    }
}
