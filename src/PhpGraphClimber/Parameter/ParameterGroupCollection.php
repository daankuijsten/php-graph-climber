<?php
declare(strict_types=1);

namespace PhpGraphClimber\Parameter;

class ParameterGroupCollection
{
    private $parameterGroups = [];

    public function add(ParameterGroup $parameterGroup): void
    {
        $this->parameterGroups[] = $parameterGroup;
    }

    public function getParameterGroups(): array
    {
        return $this->parameterGroups;
    }

    /**
     * @return ParameterGroup[]
     */
    public function getAll(): array
    {
        return $this->parameterGroups;
    }

}
