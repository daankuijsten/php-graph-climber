<?php
declare(strict_types=1);

namespace PhpGraphClimber\Parameter;

use Exception;
use PhpGraphClimber\Range\Range;

class ParameterHelper
{
    public function requiresRange(string $type): bool
    {
        return in_array($type, ['int', 'string']);
    }

    public function initialiseRange(string $type): Range
    {
        if ($type === 'int') {
            return $this->initialiseIntRange();
        }

        throw new Exception('Could not initialiseRange, type not supported: ' . json_encode($type, JSON_THROW_ON_ERROR));
    }

    private function initialiseIntRange(): Range
    {
        return new Range(
            'int',
            PHP_INT_MIN,
            PHP_INT_MAX
        );
    }

}