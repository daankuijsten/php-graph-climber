<?php
declare(strict_types=1);

namespace PhpGraphClimber\PossibleInput;

use Exception;
use PhpGraphClimber\Branch\Branch;
use PhpGraphClimber\Graph\ClassMethodGraph;
use PhpGraphClimber\Parameter\Parameter;
use PhpGraphClimber\PossibleInput\Type\PossibleInputParameterBool;
use PhpGraphClimber\PossibleInput\Type\PossibleInputParameterInt;
use PhpGraphClimber\Range\Range;
use PhpGraphClimber\Range\RangesInterface;
use PhpParser\Node\Param;

class PossibleInputCalculator
{
    public function calculatePossibleInputValues(ClassMethodGraph $classMethodGraph): void
    {
        //$possibleInputValues = $this->createPossibleInputValues($classMethodGraph);
        $this->iterateBranchesAndCalculatePossibleInputValues2($classMethodGraph);
    }

    private function createPossibleInputValues($classMethodGraph): array
    {
        $possibleInputValues = [];

        $parameters = $classMethodGraph->getMethod()->getParameters();
        foreach ($parameters as $parameter) {
            $possibleInputValue = new PossibleInputValue(
                $parameter->getName(),
                $parameter->getType(),
                $parameter->isNullable(),
                $parameter->hasDefaultValue(),
                $parameter->getDefaultValue()
            );

            $possibleInputValue->initialiseRanges();
            $possibleInputValues[] = $possibleInputValue;
        }

        return $possibleInputValues;
    }

    /**
     * Perform a Depth-first-search in the branch tree. Take all possible input parameter values and reduce them per branch requirements.
     * Also store a copy of the possible input parameters per branch.
     *
     * @deprecated
     */
    public function iterateBranchesAndCalculatePossibleInputValues(array $possibleInputValues, array $branches): void
    {
        foreach ($branches as $branch) {
            $possibleInputValueClones = [];

            $parameterGroupCollection = $branch->getParameterGroupCollection();
            foreach ($parameterGroupCollection->getAll() as $parameterGroup) {
                foreach ($parameterGroup->getAll() as $parameter) {
                    foreach ($possibleInputValues as $possibleInputValue) {
                        if ($parameter->getName() !== $possibleInputValue->getName()) {
                            continue;
                        }

                        $possibleInputValueClone = clone($possibleInputValue);

                        $this->logicalAndOnRanges($possibleInputValueClone, $parameter);
                        $this->filterIntRangesByInclusion($possibleInputValueClone, $parameter);
                        $this->deduplicateRanges($possibleInputValueClone);

                        $possibleInputValueClones[] = $possibleInputValueClone;
                    }
                }
            }

            $branch->setPossibleInputValues($possibleInputValueClones);

            $branchChildren = $branch->getBranches();
            if (!empty($branchChildren)) {
                $this->iterateBranchesAndCalculatePossibleInputValues($branch->getPossibleInputValues(), $branchChildren);
            }
        }
    }

    /**
     * Perform a Depth-first-search in the branch tree. Take all possible input parameter values and reduce them per branch requirements.
     * Also store a copy of the possible input parameters per branch.
     */
    public function iterateBranchesAndCalculatePossibleInputValues2(ClassMethodGraph $classMethodGraph): void
    {
        $branches = $classMethodGraph->getBranchesByEndStatement();

        //$possibleInputParameters = [];

        /** @var Branch $branch */
        foreach ($branches as $branch) {
            $possibleInputParametersCollection = $this->iterateBranchesAndCalculatePossibleInputValues4($classMethodGraph, $branch);
            $branch->setPossibleInputParametersCollection($possibleInputParametersCollection);
        }
        //return $possibleInputParameters;

    }

    /**
     * We take all endStatements and iterate back to the beginning of the function.
     *
     * First we initialise the value that we found in the end statement. If we don't know the type yet, we initialise it with all possible
     * types.
     *
     * TODO: When encountering variable manipulation, we possibly adapt the possibleInputValue
     *
     * When we encounter a branch, we check if the possibleInputParameter was used there and if so, we limit the possibilities even further.
     *
     * When we come to the beginning of the function, we also check the inputValues used in the signature and limit the possibilities even
     * further.
     */
    public function iterateBranchesAndCalculatePossibleInputValues3(
        ClassMethodGraph $classMethodGraph,
        Branch $branch,
        ?PossibleInputParametersCollection $possibleInputParametersCollection = null
    ): PossibleInputParametersCollection {

        // Filter by endStatement
        $endStatement = $branch->getEndStatement();

        if (is_null($possibleInputParametersCollection)) {
            $possibleInputParametersCollection = new PossibleInputParametersCollection();
        }

        /** @var PossibleInputParametersCollection $possibleInputParametersCollectionClone */
        $possibleInputParametersCollectionClone = $this->deepCopy($possibleInputParametersCollection);

        if (!is_null($endStatement)) {
            $possibleInputParameters = new PossibleInputParameters();
            $possibleInputParameters->initialisePossibleInputParameter($endStatement->getValueType(), $endStatement->getValue());

            $possibleInputParametersCollectionClone->add($possibleInputParameters);
        }

        $parameterGroupCollection = $branch->getParameterGroupCollection();

        foreach ($parameterGroupCollection->getAll() as $parameterGroup) {
            foreach ($parameterGroup->getAll() as $parameter) {

                $possibleInputParameters = $possibleInputParametersCollectionClone->findByName($parameter->getName());

                // Create possibleInputParameters if it doesn't exist
                // 1. We first tried to see if there was a variable match already in there.
                // 2. If not, we try to find the "catch-all", possibleInputParameters which is the one that has no variableName. Then we
                // copy over all the values from the "catch-all" to the new created possibleInputParameters with the variable name.
                if (!$possibleInputParameters) {
                    $possibleInputParameters = $possibleInputParametersCollectionClone->findByName(null);

                    if (!$possibleInputParameters) {
                        throw new Exception('We do not have a "catch-all" possibleInputParameters');
                    }

                    /** @var PossibleInputParameters $possibleInputParameters */
                    $possibleInputParameters = $this->deepCopy($possibleInputParameters);
                    $possibleInputParameters->setVariableName($parameter->getName());
                    $possibleInputParametersCollectionClone->add($possibleInputParameters);
                }

                $this->reducePossibleInputParameters($possibleInputParameters, $parameter);
            }
        }

        $branch->setPossibleInputParametersCollection($possibleInputParametersCollectionClone);

        $branchParent = $classMethodGraph->getParentBranch($branch);
        if (!is_null($branchParent)) {
            return $this->iterateBranchesAndCalculatePossibleInputValues3(
                $classMethodGraph,
                $branchParent,
                $possibleInputParametersCollectionClone
            );
        }

        return $possibleInputParametersCollectionClone;
    }

    /**
     * We take all endStatements and iterate back to the beginning of the function.
     *
     * First we initialise the value that we found in the end statement. If we don't know the type yet, we initialise it with all possible
     * types.
     */
    public function iterateBranchesAndCalculatePossibleInputValues4(
        ClassMethodGraph $classMethodGraph,
        Branch $branch,
        ?PossibleInputParametersCollection $possibleInputParametersCollection = null
    ): PossibleInputParametersCollection {

        // Filter by endStatement
        $endStatement = $branch->getEndStatement();

        if (is_null($possibleInputParametersCollection)) {
            $possibleInputParametersCollection = new PossibleInputParametersCollection();
        }

        /** @var PossibleInputParametersCollection $possibleInputParametersCollectionClone */
        //        $possibleInputParametersCollectionClone = $this->deepCopy($possibleInputParametersCollection);

        //        if (!is_null($endStatement)) {
        //            $possibleInputParameters = new PossibleInputParameters();
        //            $possibleInputParameters->initialisePossibleInputParameter($endStatement->getValueType(), $endStatement->getValue());
        //
        //            $possibleInputParametersCollectionClone->add($possibleInputParameters);
        //        }

        $parameterGroupCollection = $branch->getParameterGroupCollection();

        foreach ($parameterGroupCollection->getAll() as $parameterGroup) {
            foreach ($parameterGroup->getAll() as $parameter) {

                $possibleInputParameters = $possibleInputParametersCollection->findByName($parameter->getName());

                if (!$possibleInputParameters) {
                    $possibleInputParameters = new PossibleInputParameters();
                    $possibleInputParameters->initialiseAll($parameter->getName());

                    $possibleInputParametersCollection->add($possibleInputParameters);
                }

                $this->reducePossibleInputParameters($possibleInputParameters, $parameter);
            }
        }

        $branch->setPossibleInputParametersCollection($possibleInputParametersCollection);

        $branchParent = $classMethodGraph->getParentBranch($branch);
        if (!is_null($branchParent)) {
            return $this->iterateBranchesAndCalculatePossibleInputValues4(
                $classMethodGraph,
                $branchParent,
                $possibleInputParametersCollection
            );
        }

        return $possibleInputParametersCollection;
    }

    public function reducePossibleInputParameters(
        PossibleInputParameters $possibleInputParameters,
        Parameter $parameter
    ): PossibleInputParameters {
        $this->reducePossibleInputParameterInt($possibleInputParameters, $parameter);
        $this->reducePossibleInputParameterBool($possibleInputParameters, $parameter);

        return $possibleInputParameters;
    }

    private function reducePossibleInputParameterInt(
        PossibleInputParameters $possibleInputParameters,
        Parameter $parameter
    ): void {
        if ($parameter->getRanges()[0]->getType() !== PossibleInputParameters::TYPE__INT) {
            return;
        }

        $this->removeWhenTypeMismatch($possibleInputParameters, PossibleInputParameters::TYPE__INT);

        foreach ($possibleInputParameters->getAll() as $possibleInputParameter) {
            $this->logicalAndOnRanges($possibleInputParameter, $parameter);
            $this->filterIntRangesByInclusion($possibleInputParameter, $parameter);
            $this->deduplicateRanges($possibleInputParameter);
        }
    }

    private function reducePossibleInputParameterBool(
        PossibleInputParameters $possibleInputParameters,
        Parameter $parameter
    ): void {
        if ($parameter->getRanges()[0]->getType() !== PossibleInputParameters::TYPE__BOOL) {
            return;
        }

        $this->removeWhenTypeMismatch($possibleInputParameters, PossibleInputParameters::TYPE__BOOL);

        foreach ($possibleInputParameters->getAll() as $possibleInputParameter) {
            $this->filterBoolRanges($possibleInputParameter, $parameter);
        }
    }

    private function removeWhenTypeMismatch(PossibleInputParameters $possibleInputParameters, string $type): void
    {
        foreach ($possibleInputParameters->getAll() as $possibleInputParameter) {
            if ($possibleInputParameter->getType() !== $type) {
                $a = $possibleInputParameter->getType();
                $possibleInputParameters->removeByType($possibleInputParameter->getType());
            }
        }
    }

    private function deepCopy(object $object)
    {
        $allowedClasses = [
            // TODO
            'allowed_classes' => true
        ];

        return unserialize(serialize($object), $allowedClasses);
    }

    /**
     * Perform a logical AND between 2 ranges and create new ranges
     *
     * Differentiate 5 cases:
     *
     * 1. [0...9999] & [0...5] = [0...5]
     * 2. [0...9999] & [3...9999] = [3...9999]
     * 3. [0...9999] & [3...5] = [3...5]
     * 4. [3...5] & [0...9999] = [3...5]
     * 5. [3...5] & [9...10] = null (skip)
     */
    public function logicalAndOnRanges(RangesInterface $possibleInputParameter, Parameter $parameter): void
    {
        foreach ($possibleInputParameter->getRanges() as $range1) {
            foreach ($parameter->getRanges() as $range2) {

                // First check if the ranges do not overlap
                if ($range1->getLeftBoundary() > $range2->getRightBoundary()
                    || $range1->getRightBoundary() < $range2->getLeftBoundary()
                ) {
                    continue;
                }

                // Simply take all the highest left boundaries and all the lowest right boundaries
                $rangeNew = new Range(
                    'int',
                    max($range1->getLeftBoundary(), $range2->getLeftBoundary()),
                    min($range1->getRightBoundary(), $range2->getRightBoundary())
                );
                $possibleInputParameter->addRange($rangeNew);
            }
        }
    }

    /**
     * Only keep the ranges of the possibleInputValues that are completely included in one of the ranges of the parameters
     */
    public function filterIntRangesByInclusion(PossibleInputParameterInt $possibleInputParameter, Parameter $parameter): void
    {
        foreach ($possibleInputParameter->getRanges() as $range1) {

            $shouldRemove = true;
            foreach ($parameter->getRanges() as $range2) {
                if ($range1->getLeftBoundary() >= $range2->getLeftBoundary()
                    && $range1->getRightBoundary() <= $range2->getRightBoundary()
                ) {
                    $shouldRemove = false;
                }
            }

            if ($shouldRemove) {
                $possibleInputParameter->removeRange($range1);
            }
        }
    }

    public function deduplicateRanges(RangesInterface $possibleInputParameter): void
    {
        $ranges = [];
        foreach ($possibleInputParameter->getRanges() as $range) {
            $rangeKey = implode('_', [$range->getType() . $range->getLeftBoundary(), $range->getRightBoundary()]);

            if (in_array($rangeKey, $ranges, true)) {
                $possibleInputParameter->removeRange($range);
                continue;
            }

            $ranges[] = $rangeKey;
        }
    }

    /**
     * Differentiate 5 cases:
     *
     * 1. [0...9999] - [0...5] = [6...9999]
     * 2. [0...9999] - [3...9999] = [0...2]
     * 3. [0...9999] - [3...5] = [0...2], [6...9999]
     * 4. [3...5] - [0...9999] = null (remove)
     * 5. [3...5] - [9...10] = invalid (skip)
     */
    private function subtractRanges(PossibleInputValue $possibleInputValue, Parameter $parameter): void
    {
        foreach ($possibleInputValue->getRanges() as $range1) {
            foreach ($parameter->getRanges() as $range2) {
                if ($range2->getLeftBoundary() <= $range1->getLeftBoundary()
                    && $range2->getRightBoundary() >= $range1->getLeftBoundary()
                    && $range2->getRightBoundary() < $range1->getRightBoundary()
                ) {
                    $range1->setLeftBoundary($range2->getRightBoundary() + 1);
                }

                if ($range2->getRightBoundary() >= $range1->getRightBoundary()
                    && $range2->getLeftBoundary() <= $range1->getRightBoundary()
                    && $range2->getLeftBoundary() > $range1->getLeftBoundary()
                ) {
                    $range2->setRightBoundary($range2->getLeftBoundary() - 1);
                }

                if ($range2->getLeftBoundary() > $range1->getLeftBoundary()
                    && $range2->getRightBoundary() < $range1->getRightBoundary()
                ) {
                    // Use range 1 for the first and create a new one for the second
                    $range1->setRightBoundary($range2->getLeftBoundary() - 1);

                    $rangeNew = new Range(
                        'int',
                        $range2->getRightBoundary() + 1,
                        $range1->getRightBoundary(),
                    );
                    $possibleInputValue->addRange($rangeNew);
                }

                if ($range2->getLeftBoundary() <= $range1->getLeftBoundary()
                    && $range2->getRightBoundary() >= $range1->getRightBoundary()
                ) {
                    $possibleInputValue->removeRange($range1);
                }
            }
        }
    }

    /**
     * Check if there is any match in Parameter, that PossibleInputParameterBool also has. If not, remove the entry from
     * PossibleInputParameterBool
     */
    private function filterBoolRanges(PossibleInputParameterBool $possibleInputParameterBool, Parameter $parameter)
    {
        foreach($possibleInputParameterBool->getRanges() as $range1) {
            foreach($parameter->getRanges() as $range2) {
                if ($range1->getLeftBoundary() === $range2->getLeftBoundary()
                    || $range1->getLeftBoundary() === $range2->getRightBoundary()
                ) {
                    $range1->setRightBoundary($range1->getLeftBoundary());
                    continue 2;
                }
                if ($range1->getRightBoundary() === $range2->getLeftBoundary()
                    || $range1->getRightBoundary() === $range2->getRightBoundary()
                ) {
                    $range1->setLeftBoundary($range1->getRightBoundary());
                    continue 2;
                }
            }

            $possibleInputParameterBool->removeRange($range1);
        }
    }
}
