<?php
declare(strict_types=1);

namespace PhpGraphClimber\PossibleInput;

use Exception;
use PhpGraphClimber\Range\Range;

/**
 * @deprecated
 */
class PossibleInputParameter
{

    public const TYPE__INT = 'int';
    public const TYPE__FLOAT = 'float';
    public const TYPE__STRING = 'string';
    public const TYPE__ARRAY = 'array';
    public const TYPE__NULL = 'null';
    public const TYPE__OBJECT = 'object';
    public const TYPE__BOOL = 'bool';

    /**
     * @var string
     */
    private $value;

    /**
     * @var string
     */
    private $type;

    private $ranges = [];

    /**
     * @var string|null
     */
    private $name;

    public function __construct(
        string $type,
        $value = null,
        ?string $name = null
    )
    {
        $this->type = $type;
        $this->value = $value;
        $this->name = $name;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function initialiseRanges(): void
    {
        if ($this->type === 'int') {
            $this->initialiseIntRange();
            return;
        }

        if (class_exists($this->type)) {
            return;
        }

        throw new Exception('Type of input value not recognized: ' . json_encode($this->type, JSON_THROW_ON_ERROR));
    }

    private function initialiseIntRange(): void
    {
        $this->addRange(new Range(
            'int',
            PHP_INT_MIN,
            PHP_INT_MAX
        ));
    }

    public function addRange(Range $range): void
    {
        $this->ranges[] = $range;
    }

    public function getRanges(): array
    {
        return $this->ranges;
    }

    public function removeRange(Range $range): void
    {
        foreach ($this->ranges as $key => $rangeExisting) {
            if ($range === $rangeExisting) {
                unset($this->ranges[$key]);
            }
        }

        $this->ranges = array_values($this->ranges);
    }

}
