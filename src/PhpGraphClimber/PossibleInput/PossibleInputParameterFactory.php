<?php
declare(strict_types=1);

namespace PhpGraphClimber\PossibleInput;


use Exception;
use PhpGraphClimber\PossibleInput\Type\PossibleInputParameterArray;
use PhpGraphClimber\PossibleInput\Type\PossibleInputParameterBool;
use PhpGraphClimber\PossibleInput\Type\PossibleInputParameterFloat;
use PhpGraphClimber\PossibleInput\Type\PossibleInputParameterInt;
use PhpGraphClimber\PossibleInput\Type\PossibleInputParameterNull;
use PhpGraphClimber\PossibleInput\Type\PossibleInputParameterObject;
use PhpGraphClimber\PossibleInput\Type\PossibleInputParameterString;

class PossibleInputParameterFactory
{
    public function createPossibleInputParameter(string $type, $value = null): PossibleInputParameterInterface
    {
        switch ($type) {
            case PossibleInputParameters::TYPE__INT:
                return new PossibleInputParameterInt(
                    $value
                );
            case PossibleInputParameters::TYPE__FLOAT:
                return new PossibleInputParameterFloat(
                    $value
                );
            case PossibleInputParameters::TYPE__STRING:
                return new PossibleInputParameterString(
                    $value
                );
            case PossibleInputParameters::TYPE__ARRAY:
                return new PossibleInputParameterArray(
                    $value
                );
            case PossibleInputParameters::TYPE__BOOL:
                return new PossibleInputParameterBool(
                    $value
                );
            case PossibleInputParameters::TYPE__NULL:
                return new PossibleInputParameterNull(
                    $value
                );
            case PossibleInputParameters::TYPE__OBJECT:
                return new PossibleInputParameterObject(
                    $value
                );
            default:
                throw new Exception('Could not determine PossibleInputParameter type');
        }
    }
}
