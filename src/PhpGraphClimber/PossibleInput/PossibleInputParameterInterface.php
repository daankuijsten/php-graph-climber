<?php
declare(strict_types=1);

namespace PhpGraphClimber\PossibleInput;

interface PossibleInputParameterInterface
{
    public function getValue();
    public function getType(): string;
    public function initialise(): void;
}
