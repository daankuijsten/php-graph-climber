<?php
declare(strict_types=1);

namespace PhpGraphClimber\PossibleInput;

use PhpGraphClimber\CollectionInterface;

class PossibleInputParameters implements CollectionInterface
{
    public const TYPE__INT = 'int';
    public const TYPE__FLOAT = 'float';
    public const TYPE__STRING = 'string';
    public const TYPE__ARRAY = 'array';
    public const TYPE__NULL = 'null';
    public const TYPE__OBJECT = 'object';
    public const TYPE__BOOL = 'bool';

    public const TYPES = [
        self::TYPE__INT,
        self::TYPE__FLOAT,
        self::TYPE__STRING,
        self::TYPE__ARRAY,
        self::TYPE__NULL,
        self::TYPE__OBJECT,
        self::TYPE__BOOL,
    ];

    /**
     * @var string
     */
    private $name;

    /**
     * @var PossibleInputParameterFactory
     */
    private $possibleInputParameterFactory;

    private $possibleInputParameters = [];

    public function __construct()
    {
        $this->possibleInputParameterFactory = new PossibleInputParameterFactory();
    }

    public function setVariableName(string $name): void
    {
        $this->name = $name;
    }

    public function getVariableName(): ?string
    {
        return $this->name;
    }

    public function initialisePossibleInputParameter(string $type, $value = null): PossibleInputParameterInterface
    {
        $possibleInputParameter = $this->possibleInputParameterFactory->createPossibleInputParameter($type, $value);
        $possibleInputParameter->initialise();
        $this->possibleInputParameters[] = $possibleInputParameter;
        return $possibleInputParameter;
    }

    public function initialiseAll(string $name = null): void
    {
        $this->setVariableName($name);

        foreach (self::TYPES as $type) {
            $possibleInputParameter = $this->initialisePossibleInputParameter($type);
        }
    }

    public function removeByType(string $type): void
    {
        foreach ($this->possibleInputParameters as $key => $possibleInputParameter) {
            if ($possibleInputParameter->getType() !== $type) {
                continue;
            }

            unset($this->possibleInputParameters[$key]);
            $this->possibleInputParameters = array_values($this->possibleInputParameters);
        }
    }

    /**
     * @return PossibleInputParameterInterface[]
     */
    public function getAll(): array
    {
        return $this->possibleInputParameters;
    }
}
