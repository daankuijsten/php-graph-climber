<?php
declare(strict_types=1);

namespace PhpGraphClimber\PossibleInput;

use Countable;
use PhpGraphClimber\CollectionInterface;

class PossibleInputParametersCollection implements Countable, CollectionInterface
{
    private $possibleInputParameters = [];

    public function add(PossibleInputParameters $possibleInputParameters): void
    {
        $this->possibleInputParameters[] = $possibleInputParameters;
    }

    /**
     * @return PossibleInputParameters[]
     */
    public function getAll(): array
    {
        return $this->possibleInputParameters;
    }

    public function findByName(?string $name): ?PossibleInputParameters
    {
        foreach ($this->possibleInputParameters as $possibleInputParameter) {
            if ($possibleInputParameter->getVariableName() !== $name) {
                continue;
            }

            return $possibleInputParameter;
        }

        return null;
    }

    public function count(): int
    {
        return count($this->possibleInputParameters);
    }
}
