<?php
declare(strict_types=1);

namespace PhpGraphClimber\PossibleInput;

use Exception;
use PhpGraphClimber\Range\Range;

/**
 * @deprecated
 */
class PossibleInputValue
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $type;

    /**
     * @var bool
     */
    private $nullable;

    /**
     * @var bool
     */
    private $hasDefaultValue;

    private $defaultValue;

    private $ranges = [];

    public function __construct(
        string $name,
        string $type,
        bool $nullable,
        bool $hasDefaultValue,
        $defaultValue = null
    )
    {
        $this->name = $name;
        $this->type = $type;
        $this->nullable = $nullable;
        $this->hasDefaultValue = $hasDefaultValue;
        $this->defaultValue = $defaultValue;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getNullable(): bool
    {
        return $this->nullable;
    }

    public function getHasDefaultValue(): bool
    {
        return $this->hasDefaultValue;
    }

    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    public function initialiseRanges(): void
    {
        if ($this->type === 'int') {
            $this->initialiseIntRange();
            return;
        }

        if (class_exists($this->type)) {
            return;
        }

        throw new Exception('Type of input value not recognized: ' . json_encode($this->type, JSON_THROW_ON_ERROR));
    }

    private function initialiseIntRange(): void
    {
        $this->addRange(new Range(
            'int',
            PHP_INT_MIN,
            PHP_INT_MAX
        ));
    }

    public function addRange(Range $range): void
    {
        $this->ranges[] = $range;
    }

    public function getRanges(): array
    {
        return $this->ranges;
    }

    public function removeRange(Range $range): void
    {
        foreach ($this->ranges as $key => $rangeExisting) {
            if ($range === $rangeExisting) {
                unset($this->ranges[$key]);
            }
        }

        $this->ranges = array_values($this->ranges);
    }

}
