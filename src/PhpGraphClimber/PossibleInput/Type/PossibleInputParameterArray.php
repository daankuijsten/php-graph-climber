<?php
declare(strict_types=1);

namespace PhpGraphClimber\PossibleInput\Type;

use PhpGraphClimber\PossibleInput\PossibleInputParameterInterface;
use PhpGraphClimber\PossibleInput\PossibleInputParameters;
use PhpGraphClimber\Range\Range;
use PhpGraphClimber\Range\RangesInterface;

class PossibleInputParameterArray implements PossibleInputParameterInterface
{
    /**
     * @var string
     */
    private $value;

    private $ranges = [];

    public function __construct(
        ?array $value
    ) {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getType(): string
    {
        return PossibleInputParameters::TYPE__ARRAY;
    }

    public function initialise(): void
    {
        return;
    }
}
