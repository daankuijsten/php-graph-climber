<?php
declare(strict_types=1);

namespace PhpGraphClimber\PossibleInput\Type;

use PhpGraphClimber\PossibleInput\PossibleInputParameterInterface;
use PhpGraphClimber\PossibleInput\PossibleInputParameters;
use PhpGraphClimber\Range\Range;
use PhpGraphClimber\Range\RangesInterface;

class PossibleInputParameterFloat implements PossibleInputParameterInterface, RangesInterface
{
    /**
     * @var string
     */
    private $value;

    private $ranges = [];

    public function __construct(
        ?float $value
    ) {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getType(): string
    {
        return PossibleInputParameters::TYPE__FLOAT;
    }

    public function initialise(): void
    {
        $this->initialiseRanges();
    }

    public function initialiseRanges(): void
    {
        $this->addRange(new Range(
            PossibleInputParameters::TYPE__FLOAT,
            PHP_FLOAT_MIN,
            PHP_FLOAT_MAX
        ));
    }

    public function addRange(Range $range): void
    {
        $this->ranges[] = $range;
    }

    public function getRanges(): array
    {
        return $this->ranges;
    }

    public function removeRange(Range $range): void
    {
        foreach ($this->ranges as $key => $rangeExisting) {
            if ($range === $rangeExisting) {
                unset($this->ranges[$key]);
            }
        }

        $this->ranges = array_values($this->ranges);
    }

}
