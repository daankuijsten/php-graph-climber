<?php
declare(strict_types=1);

namespace PhpGraphClimber\PossibleInput\Type;

use PhpGraphClimber\PossibleInput\PossibleInputParameterInterface;
use PhpGraphClimber\PossibleInput\PossibleInputParameters;
use PhpGraphClimber\Range\Range;
use PhpGraphClimber\Range\RangesInterface;

class PossibleInputParameterInt implements PossibleInputParameterInterface, RangesInterface
{
    /**
     * @var string
     */
    private $value;

    private $ranges = [];

    private $valueResult;

    public function __construct(
        ?int $value
    ) {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getValueResult(): int
    {
        return $this->valueResult;
    }

    public function setValueResult(int $valueResult): void
    {
        $this->valueResult = $valueResult;
    }

    public function getType(): string
    {
        return PossibleInputParameters::TYPE__INT;
    }

    public function initialise(): void
    {
        $this->initialiseRanges();
    }

    public function initialiseRanges(): void
    {
        $this->addRange(new Range(
            PossibleInputParameters::TYPE__INT,
            PHP_INT_MIN,
            PHP_INT_MAX
        ));
    }

    public function addRange(Range $range): void
    {
        $this->ranges[] = $range;
    }

    public function getRanges(): array
    {
        return $this->ranges;
    }

    public function removeRange(Range $range): void
    {
        foreach ($this->ranges as $key => $rangeExisting) {
            if ($range === $rangeExisting) {
                unset($this->ranges[$key]);
            }
        }

        $this->ranges = array_values($this->ranges);
    }

}
