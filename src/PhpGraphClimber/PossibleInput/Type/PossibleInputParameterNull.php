<?php
declare(strict_types=1);

namespace PhpGraphClimber\PossibleInput\Type;

use PhpGraphClimber\PossibleInput\PossibleInputParameterInterface;
use PhpGraphClimber\PossibleInput\PossibleInputParameters;

class PossibleInputParameterNull implements PossibleInputParameterInterface
{
    /**
     * @var string
     */
    private $value;

    private $ranges = [];

    private $valueResult;

    public function __construct(
        $value
    ) {
        $this->value = $value;
        $this->setValueResult(null);
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getValueResult()
    {
        return $this->valueResult;
    }

    public function setValueResult($valueResult): void
    {
        $this->valueResult = $valueResult;
    }

    public function getType(): string
    {
        return PossibleInputParameters::TYPE__NULL;
    }

    public function initialise(): void
    {
        return;
    }
}
