<?php
declare(strict_types=1);

namespace PhpGraphClimber\PossibleInput\Type;

use PhpGraphClimber\PossibleInput\PossibleInputParameterInterface;
use PhpGraphClimber\PossibleInput\PossibleInputParameters;

class PossibleInputParameterObject implements PossibleInputParameterInterface
{
    /**
     * @var string
     */
    private $value;

    public function __construct(
        ?object $value
    ) {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getType(): string
    {
        return PossibleInputParameters::TYPE__OBJECT;
    }

    public function initialise(): void
    {
        return;
    }
}
