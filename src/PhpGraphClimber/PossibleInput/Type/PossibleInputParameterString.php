<?php
declare(strict_types=1);

namespace PhpGraphClimber\PossibleInput\Type;

use PhpGraphClimber\PossibleInput\PossibleInputParameterInterface;
use PhpGraphClimber\PossibleInput\PossibleInputParameters;
use PhpGraphClimber\Range\Range;
use PhpGraphClimber\Range\RangesInterface;

class PossibleInputParameterString implements PossibleInputParameterInterface
{
    /**
     * @var string
     */
    private $value;

    private $ranges = [];

    public function __construct(
        ?string $value
    ) {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getType(): string
    {
        return PossibleInputParameters::TYPE__STRING;
    }

    public function initialise(): void
    {
        $patterns = [];
        $patterns[] = '/[^\w$\x{0020}-\x{007E}]+/u';
        $patterns[] = '/[^\w$\x{0080}-\x{FFFF}]+/u';
    }
}
