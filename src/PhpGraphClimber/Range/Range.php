<?php
declare(strict_types=1);

namespace PhpGraphClimber\Range;

use Exception;
use PhpGraphClimber\PossibleInput\PossibleInputParameters;

class Range
{

    /**
     * @var string
     */
    private $type;

    private $leftBoundary;

    private $rightBoundary;

    public function __construct(
        string $type,
        $leftBoundary,
        $rightBoundary
    )
    {
        if (gettype($leftBoundary) !== gettype($rightBoundary)) {
            throw new Exception('Can not create a Range with the left and right boundaries of a different type');
        }

        if (!in_array($type, PossibleInputParameters::TYPES)) {
            throw new Exception('Type of Range not supported: ' . $type);
        }

        $this->type = $type;
        $this->leftBoundary = $leftBoundary;
        $this->rightBoundary = $rightBoundary;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getLeftBoundary()
    {
        return $this->leftBoundary;
    }

    public function getRightBoundary()
    {
        return $this->rightBoundary;
    }

    public function setLeftBoundary($leftBoundary): void
    {
        $this->leftBoundary = $leftBoundary;
    }

    public function setRightBoundary($rightBoundary): void
    {
        $this->rightBoundary = $rightBoundary;
    }


}
