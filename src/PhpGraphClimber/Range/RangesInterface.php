<?php
declare(strict_types=1);

namespace PhpGraphClimber\Range;

interface RangesInterface
{
    public function initialiseRanges(): void;

    public function addRange(Range $range): void;

    public function getRanges(): array;

    public function removeRange(Range $range): void;
}
