<?php
declare(strict_types=1);

namespace PhpGraphClimber\ResultInputValue;

class ResultInputValue
{
    /**
     * @var string
     */
    private $name;

    protected $value;


    public function __construct(
        string $name,
        $value
    )
    {
        $this->name = $name;
        $this->value = $value;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getValue()
    {
        return $this->value;
    }
}
