<?php
declare(strict_types=1);

namespace PhpGraphClimber\TestFunctionBlueprint;

use PhpGraphClimber\EndStatement\EndStatement;
use PhpGraphClimber\PossibleInput\PossibleInputParameterInterface;
use PhpGraphClimber\PossibleInput\PossibleInputParameters;
use PhpGraphClimber\PossibleInput\PossibleInputParametersCollection;

class TestFunctionBlueprint
{
    /**
     * @var PossibleInputParametersCollection
     */
    private $possibleInputParametersCollection;

    /**
     * @var EndStatement
     */
    private $endStatement;

    /**
     * @var string
     */
    private $functionName;

    /**
     * @var string
     */
    private $className;

    public function __construct(
        string $className,
        string $functionName,
        EndStatement $endStatement,
        PossibleInputParametersCollection $possibleInputParametersCollection = null
    )
    {
        $this->className = $className;
        $this->functionName = $functionName;
        $this->endStatement = $endStatement;
        $this->possibleInputParametersCollection = $possibleInputParametersCollection;
    }

    public function getClassName(): string
    {
        return $this->className;
    }

    public function getFunctionName(): string
    {
        return $this->functionName;
    }

    public function getEndStatement(): EndStatement
    {
        return $this->endStatement;
    }

    public function getPossibleInputParametersCollection(): PossibleInputParametersCollection
    {
        return $this->possibleInputParametersCollection;
    }

}
