<?php
declare(strict_types=1);

namespace PhpGraphClimber\TestFunctionBlueprint;

use Exception;
use PhpGraphClimber\Branch\Branch;
use PhpGraphClimber\EndStatement\EndStatementHelper;
use PhpGraphClimber\Graph\ClassMethodGraph;
use PhpGraphClimber\PossibleInput\PossibleInputParameters;
use PhpGraphClimber\PossibleInput\PossibleInputValue;
use PhpGraphClimber\Range\Range;
use PhpGraphClimber\Range\RangesInterface;
use PhpGraphClimber\ResultInputValue\ResultInputValue;

class TestFunctionBlueprintCreator
{
    /**
     * @var EndStatementHelper
     */
    private $endStatementHelper;

    public function __construct(
        EndStatementHelper $endStatementHelper
    )
    {
        $this->endStatementHelper = $endStatementHelper;
    }

    public function createTestFunctionBlueprints(ClassMethodGraph $classMethodGraph): array
    {
        $testFunctionBlueprints = [];
        $branches = $classMethodGraph->getBranchesByEndStatement();
        $methodName = $classMethodGraph->getMethod()->getName();
        $className = $classMethodGraph->getClass()->getName();

        foreach($branches as $branch) {
            if (
                $this->endStatementHelper->endStatementReturnsScalar($branch->getEndStatement())
                || $this->endStatementHelper->endStatementReturnsNull($branch->getEndStatement())
                || $this->endStatementHelper->endStatementReturnsArray($branch->getEndStatement())
                || $this->endStatementHelper->endStatementReturnsObject($branch->getEndStatement())
                || $this->endStatementHelper->endStatementThrowsException($branch->getEndStatement())
            ) {
                $testFunctionBlueprints[] = $this->createTestFunctionBlueprint($branch, $className, $methodName);
                continue;
            }

            throw new Exception('End statement return type not recognized');
        }

        return $testFunctionBlueprints;
    }

    public function createTestFunctionBlueprints2(ClassMethodGraph $classMethodGraph): array
    {
        $testFunctionBlueprints = [];
        $branches = $classMethodGraph->getBranchesByEndStatement();
        $methodName = $classMethodGraph->getMethod()->getName();
        $className = $classMethodGraph->getClass()->getName();

        foreach($branches as $branch) {
            if ($this->endStatementHelper->endStatementReturnsScalar($branch->getEndStatement())) {
                $testFunctionBlueprints[] = $this->createTestFunctionBlueprint($branch, $className, $methodName);
                continue;
            }

            if ($this->endStatementHelper->endStatementThrowsException($branch->getEndStatement())) {
                $testFunctionBlueprints[] = $this->createTestFunctionBlueprint($branch, $className, $methodName);
                continue;
            }

            throw new Exception('End statement return type not recognized');
        }

        return $testFunctionBlueprints;
    }

    private function createTestFunctionBlueprint(Branch $branch, string $className, string $methodName): TestFunctionBlueprint
    {
        $possibleInputParametersCollection = $branch->getPossibleInputParametersCollection();
        foreach ($possibleInputParametersCollection->getAll() as $possibleInputParameters) {
            foreach ($possibleInputParameters->getAll() as $possibleInputParameter) {
                if ($possibleInputParameter->getType() === PossibleInputParameters::TYPE__INT) {
                    $possibleInputParameter->setValueResult($this->getIntInRangeClosestToOne($possibleInputParameter->getRanges()));
                    continue;
                }

                if ($possibleInputParameter->getType() === PossibleInputParameters::TYPE__BOOL) {
                    $possibleInputParameter->setValueResult($this->getAnyBoolInRange($possibleInputParameter->getRanges()));
                    continue;
                }

                if ($possibleInputParameter->getType() === PossibleInputParameters::TYPE__NULL) {
                    continue;
                }
            }
        }

        // TODO check here if some valueResults are not set, this means there was an impossible combination/code path?

        return new TestFunctionBlueprint(
            $className,
            $methodName,
            $branch->getEndStatement(),
            $possibleInputParametersCollection
        );
    }

    private function getObjectResultInputValue(PossibleInputValue $possibleInputValue): ResultInputValue
    {
        return new ResultInputValue(
            $possibleInputValue->getName(),
            $this->getMethodCallResults($possibleInputValue->getRanges())
        );
    }

    /**
     * @param Range[] $ranges
     */
    private function getIntInRangeClosestToOne(array $ranges): int
    {
        $distance = [
            'number' => null,
            'distance' => PHP_INT_MAX
        ];

        foreach ($ranges as $range) {
            if ($range->getType() !== 'int') {
                continue;
            }

            if ($range->getLeftBoundary() <= 1 && $range->getRightBoundary() >= 1) {
                return 1;
            }

            if ($range->getRightBoundary() < 1 && (1- $range->getRightBoundary()) === min($distance['distance'], 1 - $range->getRightBoundary())) {
                $distance['number'] = $range->getRightBoundary();
                $distance['distance'] = 1 - $range->getRightBoundary();
                continue;
            }

            if ($range->getLeftBoundary() > 1 && ($range->getLeftBoundary() - 1) === min($distance['distance'], $range->getLeftBoundary() - 1)) {
                $distance['number'] = $range->getLeftBoundary();
                $distance['distance'] = $range->getLeftBoundary() - 1;
            }
        }

        if (is_null($distance['number'])) {
            throw new Exception('No int closest to 0 found for ranges: ' . json_encode($ranges, JSON_THROW_ON_ERROR));
        }

        return $distance['number'];
    }

    /**
     * @param Range[] $ranges
     */
    private function getAnyBoolInRange(array $ranges): ?bool
    {
        foreach ($ranges as $range) {
            $leftBoundary = $range->getLeftBoundary();
            if (isset($leftBoundary)) {
                return $leftBoundary;
            }

            $rightBoundary = $range->getRightBoundary();
            if (isset($rightBoundary)) {
                return $rightBoundary;
            }
        }
        return null;
    }

    /**
     * @param Range[] $ranges
     */
    private function getMethodCallResults(array $ranges): int
    {
        // TODO
        throw new Exception("gogo");
    }

}
