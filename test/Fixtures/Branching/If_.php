<?php
declare(strict_types=1);

namespace PhpGraphClimber\Test\Fixtures\Branching;

class If_
{
    public function is10Or12NoElse(int $number): bool
    {
        if (10 === $number) {
            return true;
        }
        if (12 === $number) {
            return true;
        }
        return false;
    }

    public function is10Or12Or13Nested(int $number): bool
    {
        if (10 === $number) {
            return true;
        } else {
            if (12 === $number) {
                return true;
            } else {
                if (13 === $number) {
                    return true;
                }
                if (16 === $number) {
                    return false;
                } else {
                    if (14 === $number) {
                        return false;
                    } else {
                        return false;
                    }
                }
            }
        }
        if (11 === $number) {
            return false;
        }
        return false;
    }

}
