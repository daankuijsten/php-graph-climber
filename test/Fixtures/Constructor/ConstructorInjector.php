<?php
declare(strict_types=1);

namespace PhpGraphClimber\Test\Fixtures\Constructor;

use PhpGraphClimber\FileHelper\PhpFileHelper;

class ConstructorInjector
{
    public function __construct(
        $foo1,
        PhpFileHelper $foo2,
        \PhpGraphClimber\Generator\MethodGenerator\MethodGenerator $foo3,
        int $foo4,
        float $foo5,
        string $foo6,
        array $foo7,
        callable $foo8,
        object $foo9
    )
    {
        $this->foo1 = $foo1;
        $this->foo2 = $foo2;
        $this->foo3 = $foo3;
        $this->foo4 = $foo4;
        $this->foo5 = $foo5;
        $this->foo6 = $foo6;
        $this->foo7 = $foo7;
        $this->foo8 = $foo8;
        $this->foo9 = $foo9;
    }
}

