<?php
declare(strict_types=1);

namespace PhpGraphClimber\Test\Fixtures\Object_;

use PhpGraphClimber\Container\Container;
use PhpGraphClimber\EndStatement\ThrowEndStatement;
use PhpGraphClimber\ExpressionMap;
use PhpGraphClimber\NameMap;
use PhpGraphClimber\Operator\OperatorHelper;

class Object_
{
    /**
     * @var OperatorHelper
     */
    private $operatorHelper;

    public function __construct(OperatorHelper $operatorHelper)
    {
        $this->operatorHelper = $operatorHelper;
    }

    public function runObjectFunctions(ThrowEndStatement $throwEndStatement): ExpressionMap
    {
        $type = $throwEndStatement->getType();

        if (10 === $type) {
            return false;
        } else {
            return true;
        }

        /*
        if (Container::create($type)) {
            return $this->handleResult($throwEndStatement, $this->doSomething());
        }

        if (!Container::create($type)) {
            return $this->handleResult($throwEndStatement, $this->doSomething());
        }

        if ($type === NameMap::ASSERTS['exception']['testFunctionShould']) {
            $result = $this->operatorHelper->getInverseOperator('foo');
        } else {
            $result = $this->operatorHelper->getNotValue('bar');
        }
        */

        return false;

        //return $this->handleResult($throwEndStatement, $result);
    }

    private function handleResult(ThrowEndStatement $throwEndStatement, string $result): ExpressionMap
    {
        return new ExpressionMap();
    }

    private function doSomething(): string
    {
        return 'baz';
    }

}
