<?php
declare(strict_types=1);

namespace PhpGraphClimber\Test\Fixtures\Return_;

use DateTime;
use Exception;
use RuntimeException;

class Return_
{
//    public function checkNumber(int $number): void
//    {
//        if (10 === $number) {
//            throw new RuntimeException('Not a number');
//        }
//    }
//
//    public function checkNumberNoCode(int $number): void
//    {
//        if (10 === $number) {
//            throw new RuntimeException('Not a number', 32, null);
//        }
//    }
//
//    public function checkNothing(): void
//    {
//        throw new Exception();
//    }

//    public function returnIntWithoutInput(): int
//    {
//        return 10;
//    }

//    public function returnInt15WithParameter(int $age): int
//    {
//        return 15;
//    }

//    public function returnInt50WithTwoParameters(int $age, int $year): int
//    {
//        return 50;
//    }

//    public function returnFloat(): float
//    {
//        return 9.4;
//    }

    public function returnTrueWithoutInput()
    {
        if (10 === $a) {
            return false;
        }
        return true;
    }

    public function returnTrueWithParameter(int $age)
    {
        return true;
    }

    public function returnTrueWithTwoParameters(int $age, int $year)
    {
        return true;
    }

    public function returnFalseWithoutInput()
    {
        return false;
    }

    public function returnFalseWithParameter(int $age)
    {
        return false;
    }

    public function returnFalseWithTwoParameters(int $age, int $year)
    {
        return false;
    }

    public function returnNullWithoutInput()
    {
        return null;
    }

    public function returnNullWithParameter(int $age)
    {
        return null;
    }

    public function returnNullWithTwoParameters(int $age, int $year)
    {
        return null;
    }

//    public function returnString(): string
//    {
//        return 'foo';
//    }

//    public function returnArray()
//    {
//        return [];
//    }

//    public function returnObject(): DateTime
//    {
//        return new DateTime('now', null);
//    }

    /* TODO
    public function returnVariable()
    {
        $a = null;
        return $a;
    }
    */

}
