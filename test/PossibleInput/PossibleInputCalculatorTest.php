<?php
declare(strict_types=1);

namespace PhpGraphClimber\Test\PossibleInput;

use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\Method;
use PhpGraphClimber\Branch\Branch;
use PhpGraphClimber\EndStatement\EndStatement;
use PhpGraphClimber\EndStatement\ValueReturn;
use PhpGraphClimber\Graph\ClassMethodGraph;
use PhpGraphClimber\Parameter\Parameter;
use PhpGraphClimber\Parameter\ParameterGroup;
use PhpGraphClimber\Parameter\ParameterGroupCollection;
use PhpGraphClimber\PossibleInput\PossibleInputCalculator;
use PhpGraphClimber\PossibleInput\PossibleInputParameterInterface;
use PhpGraphClimber\PossibleInput\PossibleInputParameters;
use PhpGraphClimber\PossibleInput\PossibleInputValue;
use PhpGraphClimber\PossibleInput\Type\PossibleInputParameterBool;
use PhpGraphClimber\PossibleInput\Type\PossibleInputParameterInt;
use PhpGraphClimber\Range\Range;
use PHPUnit\Framework\TestCase;

class PossibleInputCalculatorTest extends TestCase
{
    private PossibleInputCalculator $possibleValueCalculator;

    public function setUp(): void
    {
        parent::setUp();
        $this->possibleValueCalculator = new PossibleInputCalculator();
    }

    public function test_iterateBranchesAndCalculatePossibleInputValues2_int_shouldReduceCorrect()
    {
        $range2 = new Range(PossibleInputParameters::TYPE__INT, 10, 10);
        $parameter2 = new Parameter('foo');
        $parameter2->setValue(10);
        $parameter2->setOriginalValue(10);
        $parameter2->addRange($range2);
        $parameterGroup2 = new ParameterGroup();
        $parameterGroup2->add($parameter2);
        $parameterGroupCollection2 = new ParameterGroupCollection();
        $parameterGroupCollection2->add($parameterGroup2);
        $endStatement2 = new EndStatement(ValueReturn::TYPE, PossibleInputParameters::TYPE__BOOL, true);
        $branch2 = new Branch([0]);
        $branch2->setEndStatement($endStatement2);
        $branch2->setParameterGroupCollection($parameterGroupCollection2);

        // Empty signature
        $parameterGroupCollection1 = new ParameterGroupCollection();
        $endStatement1 = new EndStatement(ValueReturn::TYPE, PossibleInputParameters::TYPE__BOOL, false);
        $branch1 = new Branch([0]);
        $branch1->setParameterGroupCollection($parameterGroupCollection1);
        $branch1->setEndStatement($endStatement1);
        $branch1->setBranches([$branch2]);

        $classType = new ClassType;
        $method = new Method('foo');
        $classMethodGraph = new ClassMethodGraph(
            $classType,
            $method
        );

        $classMethodGraph->setBranches([$branch1]);

        $this->possibleValueCalculator->iterateBranchesAndCalculatePossibleInputValues2($classMethodGraph);

        $possibleInputParameters = $classMethodGraph
            ->getBranches()[0]
            ->getBranches()[0]
            ->getPossibleInputParametersCollection()
            ->getAll()[0]->getAll();

        $this->assertCount(1, $possibleInputParameters);
        $possibleInputParameterExpected = new PossibleInputParameterInt(null);
        $possibleInputParameterExpected->addRange(new Range('int', 10, 10));
        $this->assertEquals($possibleInputParameterExpected, $possibleInputParameters[0]);
    }

    public function test_iterateBranchesAndCalculatePossibleInputValues2_bool_shouldReduceCorrect()
    {
        $range2 = new Range(PossibleInputParameters::TYPE__BOOL, true, true);
        $parameter2 = new Parameter('foo');
        $parameter2->setValue(true);
        $parameter2->setOriginalValue(true);
        $parameter2->addRange($range2);
        $parameterGroup2 = new ParameterGroup();
        $parameterGroup2->add($parameter2);
        $parameterGroupCollection2 = new ParameterGroupCollection();
        $parameterGroupCollection2->add($parameterGroup2);
        $endStatement2 = new EndStatement(ValueReturn::TYPE, PossibleInputParameters::TYPE__BOOL, true);
        $branch2 = new Branch([0]);
        $branch2->setEndStatement($endStatement2);
        $branch2->setParameterGroupCollection($parameterGroupCollection2);

        // Empty signature
        $parameterGroupCollection1 = new ParameterGroupCollection();
        $endStatement1 = new EndStatement(ValueReturn::TYPE, PossibleInputParameters::TYPE__BOOL, false);
        $branch1 = new Branch([0]);
        $branch1->setParameterGroupCollection($parameterGroupCollection1);
        $branch1->setEndStatement($endStatement1);
        $branch1->setBranches([$branch2]);

        $classType = new ClassType;
        $method = new Method('foo');
        $classMethodGraph = new ClassMethodGraph(
            $classType,
            $method
        );

        $classMethodGraph->setBranches([$branch1]);

        $this->possibleValueCalculator->iterateBranchesAndCalculatePossibleInputValues2($classMethodGraph);

        +d($classMethodGraph);

        $possibleInputParameters = $classMethodGraph
            ->getBranches()[0]
            ->getBranches()[0]
            ->getPossibleInputParametersCollection()
            ->getAll()[0]->getAll();

        $this->assertCount(1, $possibleInputParameters);
        $possibleInputParameterExpected = new PossibleInputParameterBool(null);
        $possibleInputParameterExpected->addRange(new Range('bool', true, true));
        $this->assertEquals($possibleInputParameterExpected, $possibleInputParameters[0]);
    }

    /*
    public function test_logicalAndRanges_overflowLeftRange_createNewRanges(): void
    {
        $possibleInputValue = $this->createPossibleParameterIntWithRange(0, 9999);
        $parameter = $this->createParameterWithRange(-4, 5);

        $result = [
            $this->createRange(0, 9999),
            $this->createRange(0, 5)
        ];

        $this->possibleValueCalculator->logicalAndOnRanges($possibleInputValue, $parameter);
        $this->assertEquals($result, $possibleInputValue->getRanges());
    }

    public function test_logicalAndRanges_overflowRightRange_createNewRanges(): void
    {
        $possibleInputValue = $this->createPossibleParameterIntWithRange(0, 9999);
        $parameter = $this->createParameterWithRange(5, 10000);

        $result = [
            $this->createRange(0, 9999),
            $this->createRange(5, 9999)
        ];

        $this->possibleValueCalculator->logicalAndOnRanges($possibleInputValue, $parameter);
        $this->assertEquals($result, $possibleInputValue->getRanges());
    }

    public function test_logicalAndRanges_includedRange_createNewRanges(): void
    {
        $possibleInputValue = $this->createPossibleParameterIntWithRange(0, 9999);
        $parameter = $this->createParameterWithRange(5, 500);

        $result = [
            $this->createRange(0, 9999),
            $this->createRange(5, 500)
        ];

        $this->possibleValueCalculator->logicalAndOnRanges($possibleInputValue, $parameter);
        $this->assertEquals($result, $possibleInputValue->getRanges());
    }

    public function test_logicalAndRanges_excluded_createNewRanges(): void
    {
        $possibleInputValue = $this->createPossibleParameterIntWithRange(0, 9999);
        $parameter = $this->createParameterWithRange(-9, -1);

        $result = [
            $this->createRange(0, 9999),
        ];

        $this->possibleValueCalculator->logicalAndOnRanges($possibleInputValue, $parameter);
        $this->assertEquals($result, $possibleInputValue->getRanges());
    }

    public function test_logicalAndRanges_onEdgeRange_createNewRanges(): void
    {
        $possibleInputValue = $this->createPossibleParameterIntWithRange(0, 9999);
        $parameter = $this->createParameterWithRange(-9, 0);

        $result = [
            $this->createRange(0, 9999),
            $this->createRange(0, 0)
        ];

        $this->possibleValueCalculator->logicalAndOnRanges($possibleInputValue, $parameter);
        $this->assertEquals($result, $possibleInputValue->getRanges());
    }

    public function test_logicalAndRanges_onEdgeWithZeros_createNewRanges(): void
    {
        $possibleInputValue = $this->createPossibleParameterIntWithRange(0, 0);
        $parameter = $this->createParameterWithRange(-9, 0);

        $result = [
            $this->createRange(0, 0),
            $this->createRange(0, 0)
        ];

        $this->possibleValueCalculator->logicalAndOnRanges($possibleInputValue, $parameter);
        $this->assertEquals($result, $possibleInputValue->getRanges());
    }

    public function test_logicalAndRanges_multipleRanges_createNewRanges(): void
    {
        $possibleInputValue = $this->createPossibleParameterInt();
        $possibleInputValue->addRange($this->createRange(0, 5));
        $possibleInputValue->addRange($this->createRange(10, 20));
        $possibleInputValue->addRange($this->createRange(50, 100));

        $parameter = $this->createParameter();
        $parameter->addRange($this->createRange(-4, 2));
        $parameter->addRange($this->createRange(3, 8));
        $parameter->addRange($this->createRange(20, 100));

        $result = [
            $this->createRange(0, 5),
            $this->createRange(10, 20),
            $this->createRange(50, 100),
            $this->createRange(0, 2),
            $this->createRange(3, 5),
            $this->createRange(20, 20),
            $this->createRange(50, 100),
        ];

        $this->possibleValueCalculator->logicalAndOnRanges($possibleInputValue, $parameter);
        $this->assertEquals($result, $possibleInputValue->getRanges());
    }

    public function test_filterIntRangesByInclusion_includedRange_filterRanges(): void
    {
        $possibleInputValue = $this->createPossibleParameterInt();
        $possibleInputValue->addRange($this->createRange(0, 5));
        $possibleInputValue->addRange($this->createRange(10, 20));
        $possibleInputValue->addRange($this->createRange(50, 100));

        $parameter = $this->createParameter();
        $parameter->addRange($this->createRange(0, 8));
        $parameter->addRange($this->createRange(40, 60));
        $parameter->addRange($this->createRange(60, 100));

        $result = [
            $this->createRange(0, 5),
        ];

        $this->possibleValueCalculator->filterIntRangesByInclusion($possibleInputValue, $parameter);
        $this->assertEquals($result, $possibleInputValue->getRanges());
    }

    public function test_filterIntRangesByInclusion_includedRangeAll_filterRanges(): void
    {
        $possibleInputValue = $this->createPossibleParameterInt();
        $possibleInputValue->addRange($this->createRange(0, 5));
        $possibleInputValue->addRange($this->createRange(10, 20));
        $possibleInputValue->addRange($this->createRange(50, 100));

        $parameter = $this->createParameter();
        $parameter->addRange($this->createRange(0, 8));
        $parameter->addRange($this->createRange(60, 100));
        $parameter->addRange($this->createRange(5, 200));

        $result = [
            $this->createRange(0, 5),
            $this->createRange(10, 20),
            $this->createRange(50, 100),
        ];

        $this->possibleValueCalculator->filterIntRangesByInclusion($possibleInputValue, $parameter);
        $this->assertEquals($result, $possibleInputValue->getRanges());
    }

    public function test_filterIntRangesByInclusion_includedMinus_filterRanges(): void
    {
        $possibleInputValue = $this->createPossibleParameterInt();
        $possibleInputValue->addRange($this->createRange(-1000, 5));
        $possibleInputValue->addRange($this->createRange(-100, 20));
        $possibleInputValue->addRange($this->createRange(-50, 100));

        $parameter = $this->createParameter();
        $parameter->addRange($this->createRange(-900, 200));

        $result = [
            $this->createRange(-100, 20),
            $this->createRange(-50, 100),
        ];

        $this->possibleValueCalculator->filterIntRangesByInclusion($possibleInputValue, $parameter);
        $this->assertEquals($result, $possibleInputValue->getRanges());
    }

    public function test_iterateBranchesAndCalculatePossibleInputValues_withOneBranch_shouldCalculatePossibleInputValues(): void
    {
        $possibleInputValue = $this->createPossibleParameterInt();
        $possibleInputValue->addRange($this->createRange(0, 400));

        $parameterCollection1_1 = $this->createParameterGroupCollection([
            [-1, 300],
            [400, 600],
        ]);

        $parameterCollection2_1 = $this->createParameterGroupCollection([
            [40, 60],
            [300, 500],
        ]);

        $parameterCollection2_2 = $this->createParameterGroupCollection([
            [-1, 1],
            [2, 2],
        ]);

        $parameterCollection3_1 = $this->createParameterGroupCollection([
            [30, 32]
        ]);

        $parameterCollection3_2 = $this->createParameterGroupCollection([
            [-9999, 9999]
        ]);

        $branch1_1 = $this->createBranch([1, 1], null);
        $branch2_1 = $this->createBranch([2, 1], [1, 1]);
        $branch2_2 = $this->createBranch([2, 2], [1, 1]);
        $branch3_1 = $this->createBranch([3, 1], [2, 1]);
        $branch3_2 = $this->createBranch([3, 2], [2, 1]);

        $branch1_1->setParameterGroupCollection($parameterCollection1_1);
        $branch2_1->setParameterGroupCollection($parameterCollection2_1);
        $branch2_2->setParameterGroupCollection($parameterCollection2_2);
        $branch3_1->setParameterGroupCollection($parameterCollection3_1);
        $branch3_2->setParameterGroupCollection($parameterCollection3_2);

        $branch2_1->addBranch($branch3_1);
        $branch2_1->addBranch($branch3_2);
        $branch1_1->addBranch($branch2_1);
        $branch1_1->addBranch($branch2_2);

        $this->possibleValueCalculator->iterateBranchesAndCalculatePossibleInputValues([$possibleInputValue], [$branch1_1]);

        $branch2_1_result = $branch1_1->getBranches()[0];
        $branch2_2_result = $branch1_1->getBranches()[1];
        $branch3_1_result = $branch2_1_result->getBranches()[0];
        $branch3_2_result = $branch2_1_result->getBranches()[1];

        $this->assertEquals($branch1_1->getPossibleInputValues()[0]->getRanges()[0], $this->createRange(0, 300));
        $this->assertEquals($branch1_1->getPossibleInputValues()[0]->getRanges()[1], $this->createRange(400, 400));
        $this->assertFalse(isset($branch1_1->getPossibleInputValues()[0]->getRanges()[2]));
        $this->assertFalse(isset($branch1_1->getPossibleInputValues()[1]));

        $this->assertEquals($branch2_1_result->getPossibleInputValues()[0]->getRanges()[0], $this->createRange(400, 400));
        $this->assertEquals($branch2_1_result->getPossibleInputValues()[0]->getRanges()[1], $this->createRange(40, 60));
        $this->assertEquals($branch2_1_result->getPossibleInputValues()[0]->getRanges()[2], $this->createRange(300, 300));
        $this->assertFalse(isset($branch2_1_result->getPossibleInputValues()[0]->getRanges()[3]));
        $this->assertFalse(isset($branch2_1_result->getPossibleInputValues()[1]));

        $this->assertEquals($branch2_2_result->getPossibleInputValues()[0]->getRanges()[0], $this->createRange(0, 1));
        $this->assertEquals($branch2_2_result->getPossibleInputValues()[0]->getRanges()[1], $this->createRange(2, 2));
        $this->assertFalse(isset($branch2_2_result->getPossibleInputValues()[0]->getRanges()[2]));
        $this->assertFalse(isset($branch2_2_result->getPossibleInputValues()[1]));

        $this->assertFalse(isset($branch3_1_result->getPossibleInputValues()[0]->getRanges()[0]));

        $this->assertEquals($branch3_2_result->getPossibleInputValues()[0]->getRanges()[0], $this->createRange(400, 400));
        $this->assertEquals($branch3_2_result->getPossibleInputValues()[0]->getRanges()[1], $this->createRange(40, 60));
        $this->assertEquals($branch3_2_result->getPossibleInputValues()[0]->getRanges()[2], $this->createRange(300, 300));
        $this->assertFalse(isset($branch3_2_result->getPossibleInputValues()[0]->getRanges()[3]));
        $this->assertFalse(isset($branch3_2_result->getPossibleInputValues()[1]));
    }




    private function createPossibleParameterIntWithRange(int $leftBoundary, int $rightBoundary): PossibleInputParameterInterface
    {
        $possibleInputValue = $this->createPossibleParameterInt();

        $range = $this->createRange($leftBoundary, $rightBoundary);

        $possibleInputValue->addRange($range);
        return $possibleInputValue;
    }

    private function createParameterWithRange(int $leftBoundary, int $rightBoundary): Parameter
    {
        $parameter = $this->createParameter();

        $range = $this->createRange($leftBoundary, $rightBoundary);

        $parameter->addRange($range);
        return $parameter;
    }

    private function createRange(int $leftBoundary, int $rightBoundary): Range
    {
        return new Range(
            'int',
            $leftBoundary,
            $rightBoundary
        );
    }

    private function createPossibleParameterInt(): PossibleInputParameterInt
    {
        return new PossibleInputParameterInt(
            1
        );
    }

    private function createParameter(): Parameter
    {
        return new Parameter(
            'foo'
        );
    }

    private function createBranch(array $nodeNumbers, ?array $nodeNumbersParentBranch): Branch
    {
        return new Branch(
            $nodeNumbers,
            $nodeNumbersParentBranch
        );
    }

    private function createParameterGroupCollection(array $leftRightBoundaries): ParameterGroupCollection
    {
        $parameter = $this->createParameter();
        foreach($leftRightBoundaries as $leftRightBoundary) {
            $parameter->addRange($this->createRange($leftRightBoundary[0], $leftRightBoundary[1]));
        }

        $parameterGroup = new ParameterGroup;
        $parameterGroup->add($parameter);

        $parameterGroupCollection = new ParameterGroupCollection;
        $parameterGroupCollection->add($parameterGroup);

        return $parameterGroupCollection;
    }
    */
}
