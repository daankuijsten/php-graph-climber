<?php
declare(strict_types=1);

namespace PhpGraphClimber\Test\TestFunctionBlueprint;

use Nette\PhpGenerator\Method;
use Nette\PhpGenerator\Parameter;
use PhpGraphClimber\Graph\ClassMethodGraph;
use PhpGraphClimber\TestFunctionBlueprint\TestFunctionBlueprintCreator;
use PHPUnit\Framework\TestCase;

class TestFunctionBlueprintTest extends TestCase
{
    /**
     * @var TestFunctionBlueprintCreator
     */
    private $testFunctionBlueprintCreator;

    public function setUp(): void
    {
        parent::setUp();

        $this->testFunctionBlueprintCreator = new TestFunctionBlueprintCreator();
    }

    public function test_createTestFunctionBlueprints_withSinglePossibleInputValue_createOneBlueprint(): void
    {
        $methodBody = '
            if (10 === $int) {
                return true;
            } else {
                return false;
            }
        ';

        $method = new Method('is10');
        $method->setBody($methodBody);
        $method->setParameters([new Parameter('int')]);
        $classMethodGraph = new ClassMethodGraph($method);

        $testFunctionBlueprints = $this->testFunctionBlueprintCreator->createTestFunctionBlueprints($classMethodGraph);
        var_dump($testFunctionBlueprints);

    }
}
